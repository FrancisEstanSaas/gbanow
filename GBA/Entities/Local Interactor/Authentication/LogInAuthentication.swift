//
//  LogInAuthentication.swift
//  GBA
//
//  Created by EDI on 6/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import RealmSwift
import LocalAuthentication

protocol SetUserLoginInfo {
    func addPrimaryLogInUser()
    func setNewPrimary(userName: String, userPassword: String)
}

//Set proper name for this realm object etc CurrentUser?
class UserKeyInfo: Object {
    @objc dynamic var userID: String?  = "0" // : String?  = "Primary"
    @objc dynamic var userNumber: String? 
    @objc dynamic var userPassword: String?
    @objc dynamic var userPIN: String?
    
    override static func primaryKey() -> String {
        return "userID"
    }
}

extension UserKeyInfo: LocalEntityProcess{
}


protocol RegisteredUserLogin {
    func addPrimaryLogInUser()
    func setNewPrimary(userName: String, userPassword: String)
}

class RegisteredUserInfo: Object {
    @objc dynamic var registeredUserID = "0" //: String? = "RegisteredUser"
    @objc dynamic var userID: String?  = "0"
    @objc dynamic var logInType: String?  = "0"
    //@objc dynamic var userFullName: String?
    
    override static func primaryKey() -> String {
        return "registeredUserID"
    }
}

extension RegisteredUserInfo: LocalEntityProcess{
}
