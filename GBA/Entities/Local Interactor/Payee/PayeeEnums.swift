//
//  PayeeEnums.swift
//  GBA
//
//  Created by Republisys on 22/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

enum PayeeStatus: Int {
    case inactive   = 0
    case active     = 1
    
    var stringValue: String{
        get{
            switch self {
            case .active: return "Active"
            case .inactive: return "Inactive"
            }
        }
    }
}
