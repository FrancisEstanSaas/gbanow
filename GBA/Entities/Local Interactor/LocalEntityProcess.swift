    //
//  LocalEntityProcess.swift
//  GBA
//
//  Created by Francis Bergonia on 15/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import RealmSwift

protocol LocalEntityProcess: class{
    func append()
}

extension LocalEntityProcess where Self: Object{
    @discardableResult
    func rewrite()->Self{
        do{
            try self.truncate()
            try GBARealm.write { GBARealm.create(Self.self, value: self, update: true) }
        }
        catch{ print(error.localizedDescription) }
        return self
    }
    
    func append(){
        do{ try GBARealm.write { GBARealm.create(Self.self, value: self, update: true) } }
        catch{ print(error.localizedDescription) }
    }
    
    func search(by predicate: NSPredicate)->Results<Self>{
        return GBARealm.objects(Self.self).filter(predicate)
    }
    
    func truncate() throws{
        let obj = GBARealm.objects(Self.self)
        try GBARealm.write { GBARealm.delete(obj) }
    }
}
