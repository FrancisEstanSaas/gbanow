//
//  Module.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/25/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit

struct Module{
    let name: String
    var activity: Activity? = nil
    
    var board: UIStoryboard{ get{ return UIStoryboard(name: self.name, bundle: nil) } }
    
    init(name: String, activity: ViewControllerIdentifier?) {
        self.name = name
        
        guard let act = activity else { return }
        self.activity = Activity(identifier: act, in: self)
    }
}
