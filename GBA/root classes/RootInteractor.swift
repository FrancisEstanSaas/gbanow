//
//  InteractorContract.swift
//  ArchitectureLayout
//
//  Created by Emmanuel Albania on 10/16/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import RealmSwift

protocol InteractorInterface: class{
    
}

extension InteractorInterface{
    
}

class RootRemoteInteractor: InteractorInterface{
    
}

class RootLocalInteractor: InteractorInterface{
    func fetchList<Entity: Object>(entity: Entity.Type)->[Entity]{
        let result = GBARealm.objects(Entity.self)
        var list: [Entity] = [Entity]()
        result.forEach{ list.append($0) }
        return list
    }
}
