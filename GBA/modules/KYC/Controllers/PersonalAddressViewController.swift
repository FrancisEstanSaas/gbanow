//
//  PersonalAddressViewController.swift
//  GBA
//
//  Created by EDI on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class PersonalAddressViewController: KYCModuleViewController {
    
    @IBOutlet weak var currentStreetAddTextField: GBATitledTextField!
    @IBOutlet weak var currentStateTextField: GBATitledTextField!
    @IBOutlet weak var currentCityTextField: GBATitledTextField!
    @IBOutlet weak var currentZipCodeTextField: GBATitledTextField!
    @IBOutlet weak var currentCountry_dropDown: GBAPopOverButton!
    
    //Permanent Address
    @IBOutlet weak var presentStreetAddTextField: GBATitledTextField!
    @IBOutlet weak var presentStateTextField: GBATitledTextField!
    @IBOutlet weak var presentCityTextField: GBATitledTextField!
    @IBOutlet weak var presentZipCodeTextField: GBATitledTextField!
    @IBOutlet weak var presentCountry_dropDown: GBAPopOverButton!
    
    
    
    private var selectedCountry: Countries = .Philippines
    
    var currentPresenter: KYCPagePresenter{
        guard let prsntr = self.presenter as? KYCPagePresenter
            else{ fatalError("Error in parsing presenter for KYCViewController") }
        return prsntr
    }

    
    override func viewDidLoad() {
        
        let defaultCountry = Countries.Philippines
    
        //Current
        self.currentStreetAddTextField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: currentStateTextField)
            //.set(titledFieldDelegate: self)
            .set(placeholder: "Street No./ Building")
            //.set(validation: .name(message: "Invalid first name"))
        
        
        self.currentStateTextField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: currentCityTextField)
            .set(placeholder: "State")
            //.set(titledFieldDelegate: self)

        
        self.currentCityTextField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: currentStateTextField)
            //.set(titledFieldDelegate: self)
            .set(placeholder: "City")

        
        self.currentZipCodeTextField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: currentStateTextField)
            //.set(titledFieldDelegate: self)
            .set(placeholder: "Zip Code")
        
        
        self.currentCountry_dropDown
            .set(parent: self)
            .set(placeholder: "Country")
            .set(text: defaultCountry.name)
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
        
        //Present
        self.presentStreetAddTextField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: currentStateTextField)
            //.set(titledFieldDelegate: self)
            .set(placeholder: "Street No./ Building")
        //.set(validation: .name(message: "Invalid first name"))
        
        
        self.presentStateTextField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            //.set(next: email_textField)
            .set(placeholder: "State")
        //.set(titledFieldDelegate: self)
        
        
        self.presentCityTextField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: currentStateTextField)
            //.set(titledFieldDelegate: self)
            .set(placeholder: "City")
        
        
        self.presentZipCodeTextField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: currentStateTextField)
            //.set(titledFieldDelegate: self)
            .set(placeholder: "Zip Code")
        
        
        self.presentCountry_dropDown
            .set(parent: self)
            .set(placeholder: "Country")
            .set(text: defaultCountry.name)
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
    
        self.addBackButton()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.title = "Registration"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .black
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        self.view.backgroundColor = GBAColor.white.rawValue
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    override func backBtn_tapped() {
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel your registration?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil) }
    
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        self.currentPresenter.wireframe.navigate(to: .PersonalSalaryVC)
    }
    
    
    
}

extension PersonalAddressViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}


extension PersonalAddressViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBACountryCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        
        presentCountry_dropDown.set(icon: cell.flagImage.image!)
        presentCountry_dropDown.set(text: cell.countryLabel.text!)
        currentCountry_dropDown.set(icon: cell.flagImage.image!)
        currentCountry_dropDown.set(text: cell.countryLabel.text!)
        self.selectedCountry = cell.country
        
    }
}






