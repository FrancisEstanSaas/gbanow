//
//  KYCModuleViewController.swift
//  GBA
//
//  Created by EDI on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class KYCModuleViewController: RootViewController, GBAFocusableInputViewDelegate{
    
    
    var presenter: KYCRootPresenter{
        get {
            let prsntr = self._presenter as! KYCRootPresenter
            return prsntr
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apply(gradientLayer: .shadedBlueGreen)
    }
    
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
}


