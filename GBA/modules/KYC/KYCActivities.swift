//
//  KYCActivities.swift
//  GBA
//
//  Created by EDI on 28/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
//To be filled :D
enum KYCActivities: ViewControllerIdentifier{

    case PersonalDetailsVC = "PersonalDetailsVC"
    case PersonalAddressVC = "PersonalAddressVC"
    case PersonalSalaryVC = "PersonalSalaryVC"
    case GovtIDVC = "GovtIDVC"
    case BillingInfoVC = "BillingInfoVC"
    

    func getPresenter(with viewController: KYCModuleViewController, and wireframe: KYCWireframe)->RootPresenter?{

        switch self {
        case .PersonalDetailsVC:
            return KYCPagePresenter(wireframe: wireframe, view: viewController)
        case .PersonalAddressVC:
            return KYCPagePresenter(wireframe: wireframe, view: viewController)
        case .PersonalSalaryVC:
            return KYCPagePresenter(wireframe: wireframe, view: viewController)
        case .GovtIDVC:
            return KYCPagePresenter(wireframe: wireframe, view: viewController)
        case .BillingInfoVC:
            return KYCPagePresenter(wireframe: wireframe, view: viewController)
        }
    }

}

