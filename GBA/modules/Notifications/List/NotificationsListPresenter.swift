//
//  NotificationsListPresenter.swift
//  GBA
//
//  Created by Gladys Prado on 12/3/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import UIKit

protocol DataReceivedFromNotifications{
    func didReceiveResponse(code: String)
}

class NotificationsListPresenter: NotificationsRootPresenter{
    var notificationsHolder:[[String: Any]] = [[String: Any]]()
    
    func fetchRemoteTransactionHistoryList(in page: Int, successHandler: @escaping (()->Void)){
        self.interactor.remote.getAllNotifications(successHandler: { (json, statusCode) in
            switch statusCode{
            case .noContent: break
            case .fetchSuccess:
                self.notificationsHolder.removeAll()
                guard let transactions = json["data"] as? [[String: Any]] else {
                    return
                }
                
                for transaction in transactions {
                    let holder = transaction as [String: Any]
                    self.notificationsHolder.append(holder)
                }
                successHandler()
                
            default: break
            }
        })
    }
}
