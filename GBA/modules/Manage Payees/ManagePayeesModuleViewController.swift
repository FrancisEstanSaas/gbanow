//
//  ManagePayeesModuleViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class ManagePayeesModuleViewController: RootViewController{
    
    var presenter: ManagePayeesRootPresenter{
        get {
            let prsntr = self._presenter as! ManagePayeesRootPresenter
            return prsntr
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

}

