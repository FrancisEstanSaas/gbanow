//
//  EditPayeePresenter.swift
//  GBA
//
//  Created by Republisys on 30/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit

class EditPayeePresenter: ManagePayeesRootPresenter{
    
    var mode: EditPayeeMode = .read
    fileprivate var payee: Payee? = nil
    
    func addRecipient(payee: Payee){
        self.interactor.remote.addPayee(id: payee.uid.StringValue, nickname: payee.nickname!) { (reply, replyCode) in
            print(reply)
            print(replyCode)
            switch replyCode{
            case .dataCreated:
                guard let message = reply["message"] as? String else{
                    self.presentToast(title: "Not Found", message: "Process was successful but the message was not found.")
                    return
                }
                self.presentToast(title: "Success", message: message)
            default: break
            }
        }
    }
    
    func update(_ nickname: String){
        guard let id = payee?.uid,
         let _view = self.view as? EditPayeeViewController else { return }
        
        self.interactor.remote.UpdatePayee(nickname, by: id) { (reply, replyCode) in
            guard let message = reply["message"] as? String else { return }
            
            switch replyCode{
            case .fetchSuccess:
                self.showAlert(with: "SUCCESS", message: message, completion: { ( ) } )
                try! GBARealm.write {
                    self.payee?.setValue(nickname, forKey: "nickname")
                }
                self.set(mode: .read)
                _view.setEditMode()
            default: break
            }
        }
    }
    
    func remove(){
        guard let id = payee?.uid else { return }
        
        self.interactor.remote.deletePayee(id: id) { (reply, replyCode) in
            
            switch replyCode{
            case .fetchSuccess:
                guard let message = reply ["message"] as? String else{ return }
                self.showAlert(with: "", message: message, completion: {
                    self.view.navigationController?.popToRootViewController(animated: true)
                })
                
            case .badRequest:
                guard let message = reply ["message"] as? String else { return }
                self.showAlert(with: "Warning", message: message, completion: {})
                
            default: break
            }
        }
    }
    
    func getPayee()->Payee?{
        return payee
    }
    
    private func presentToast(title: String?, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.view.present(alert, animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (_) in
            alert.dismiss(animated: true, completion: {
                self.view.navigationController?.popToRootViewController(animated: true)
            })
        }
    }
    
}

extension EditPayeePresenter{
    @discardableResult
    func set(mode: EditPayeeMode)->Self{
        self.mode = mode
        return self
    }
    
    @discardableResult
    func set(payee: Payee?)->Self{
        self.payee = payee
        return self
    }
}
