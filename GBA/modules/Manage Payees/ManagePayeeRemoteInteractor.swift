//
//  ManagePayeeRemoteInteractor.swift
//  GBA
//
//  Created by Republisys on 22/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

fileprivate enum APICalls: Router{
    
    var baseComponent: String { get { return "/public" } }
    
    case fetchPayee
    case checkPayee(mobile: String)
    case invitePayee(mobile: String)
    case addPayee(id: String, nickname: String)
    case updatePayee(nickname: String, id: Int)
    case deletePayee(id: Int)
    
    var route: Route{
        switch self {
        case .fetchPayee:
            return Route(   method              : .get,
                            suffix              : "/payees",
                            parameters          : nil,
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .checkPayee(let mobile):
            return Route(   method              : .post,
                            suffix              : "/payee/check",
                            parameters          :["mobile" : mobile],
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .invitePayee(let mobile):
            return Route(   method              : .post,
                            suffix              : "/invite/user",
                            parameters          : ["mobile" : mobile],
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .addPayee(let id,let nickname):
            return Route(   method              : .post,
                            suffix              : "payees",
                            parameters          : ["user_id"  : id,
                                                 "nickname"   : nickname],
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .updatePayee(let name, let id):
            return Route(   method              : .patch,
                            suffix              : "payees/\(id)",
                            parameters          : ["nickname" : name],
                            waitUntilFinished   : true,
                            nonToken            : false)
        case .deletePayee(let id):
            return Route(method: .delete,
                         suffix: "payees",
                         parameters: ["payee_id": id],
                         waitUntilFinished: true,
                         nonToken: false)
        }
    } 
}

class ManagePayeeRemoteInteractor: RootRemoteInteractor{
    
    func FetchPayeeList(successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.fetchPayee, successHandler: {
            (reply, statusCode) in
            print(reply,statusCode)
            successHandler(reply, statusCode)
        })
    }
    
    func CheckPayee(with mobile: String, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.checkPayee(mobile: mobile), successHandler: successHandler)
    }
    
    func InvitePayee(with mobile: String, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.invitePayee(mobile: mobile), successHandler: successHandler)
    }
    
    func addPayee(id: String, nickname: String, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.addPayee(id: id, nickname: nickname), successHandler: successHandler)
    }
    
    func UpdatePayee(_ nickname: String, by id: Int, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.updatePayee(nickname: nickname, id: id), successHandler: successHandler)
    }
    
    func deletePayee(id: Int, successHandler: @escaping ((JSON, ServerReplyCode)->Void)){
        NetworkingManager.request(APICalls.deletePayee(id: id), successHandler: successHandler)
    }
}
