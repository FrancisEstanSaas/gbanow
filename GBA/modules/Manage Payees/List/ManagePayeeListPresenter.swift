//
//  ManagePayeeListPresenter.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

class ManagePayeeListPresenter: ManagePayeesRootPresenter {
    
    func fetchRemotePayeeList(in page: Int, successHandler: @escaping (()->Void)){
        var payeeList: [Payee]? = [Payee]()
        
        self.interactor.remote.FetchPayeeList(successHandler: { (json, statusCode) in
            switch statusCode{
            case .noContent: break
            case .fetchSuccess:
                try! Payee().truncate()
                
                guard let rowCount = json["rows"] as? Int,
                    let payees = json["data"] as? [[String: Any]] else{
                    return
                }
                print(rowCount)
                
                payees.forEach{ payeeList?.append(Payee(json: $0)) }
                
                payeeList?.forEach{ $0.append() }
                
                successHandler()
            default: break
            }
        })
    }
    
    func fetchLocalPayeeList()->[Payee]?{
        return self.interactor.local.PayeeList
    }
    
}
