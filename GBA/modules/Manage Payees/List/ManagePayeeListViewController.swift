//
//  ManagePayeeListViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import CryptoSwift

class ManagePayeeListViewController: ManagePayeesModuleViewController{
    
    @IBOutlet weak var payeeList_tableView: UITableView!
    @IBOutlet weak var emptyStateHolder: UIView!
    fileprivate var payeeList: [Payee] = [Payee]()
    
    
    private var currentPresenter: ManagePayeeListPresenter{
        guard let prsntr = self.presenter as? ManagePayeeListPresenter else{
            fatalError("Error parsing presenter in ManagePayeeListController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        payeeList_tableView.delegate = self
        payeeList_tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Manage Payees"
        
        guard let nav = self.navigationController else {
            fatalError("Navigation Controller was not set for ManagePayeeListViewController")
        }
        
        nav.isNavigationBarHidden = false
        
        let addNewRecipient = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addNewRecipientBTN_tapped))
        self.navigationItem.rightBarButtonItem = addNewRecipient
        
        payeeList_tableView.deselectRow(at: IndexPath(item: 1, section: 1), animated: true)
        
        self.reloadTableView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc func addNewRecipientBTN_tapped(){
        self.presenter.wireframe.navigate(to: .AddRecipientscreen)
    }
    
    override func backBtn_tapped() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension ManagePayeeListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: self.view.frame.width, height: 52)))
        sectionView.backgroundColor = GBAColor.lightGray.rawValue
        
        let date = UILabel()
            .set(color: GBAColor.darkGray.rawValue)
            .set(alignment: .left)
            .set(fontStyle: GBAText.Font.main(12).rawValue)
            .add(to: sectionView)
            .set(value: "RECIPIENTS")
        
        date.topAnchor.constraint(equalTo: sectionView.topAnchor, constant: 10).Enable()
        date.widthAnchor.constraint(equalTo: sectionView.widthAnchor, multiplier: 9/10).Enable()
        date.centerXAnchor.constraint(equalTo: sectionView.centerXAnchor).Enable()
        date.bottomAnchor.constraint(equalTo: sectionView.bottomAnchor).Enable()
        
        return sectionView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? PayeesCellView,
            let payee = cell.payee else { return }
        
        self.presenter.wireframe.navigate(to: .EditPayeescreen(.read, payee))
    }
}

extension ManagePayeeListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        (self.payeeList.count > 0) ? (self.emptyStateHolder.isHidden = true) : (self.emptyStateHolder.isHidden = false)
        return self.payeeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let payee = payeeList[indexPath.row]
        
        guard let cell = Bundle.main.loadNibNamed("PayeesCellView", owner: self, options: nil)?.first as? PayeesCellView else {
            fatalError("Error in gathering cellview in nib files")
        }
        
        return cell.set(payee: payee)
    }
    
}


//Functions
extension ManagePayeeListViewController{
    
    func encryptUserData(key: String, iv: String, userData: String) -> String {
        let data = userData.data(using: .utf8)!
        let encrypted = try! AES(key: key, iv: iv).encrypt([UInt8](data))
        let encryptedData = Data(encrypted)
        return encryptedData.base64EncodedString()
    }
    
    func decryptUserData(key: String, iv: String, userData: String) -> String {
        let data = Data(base64Encoded: userData)!
        let decrypted = try! AES(key: key, iv: iv).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
    }
    
    fileprivate func reloadTableView(){
        self.currentPresenter.fetchRemotePayeeList(in: 0, successHandler: {
            self.payeeList = self.currentPresenter.fetchLocalPayeeList() ?? [Payee]()
            self.payeeList_tableView.reloadData()
        })
    }
    
    
    
    
}

