//
//  DashboardActivities.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/20/17.
//  Copyright © 2017 Republisys. All rights reserved.
//


enum DashboardActivities: ViewControllerIdentifier{
    case Dashboardscreen = "Dashboardscreen"
    
    func getPresenter(with viewController: DashboardModuleViewController, and wireframe: DashboardWireframe)->RootPresenter?{
        
        switch self {
        case .Dashboardscreen:
            return DashboardRootPresenter(wireframe: wireframe, view: viewController)
//        default: break
        }
    }
}
