//
//  DashboardRootPresenter.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/20/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class DashboardRootPresenter: RootPresenter{
    
    var wireframe: DashboardWireframe
    var view: DashboardModuleViewController
    var interactor: (local: DashboardLocalInteractor, remote: DashboardRemoteInteractor) = (DashboardLocalInteractor(), DashboardRemoteInteractor())
    var transactionHistoryHolder:[[String: Any]] = [[String: Any]]()
    
    init(wireframe: DashboardWireframe, view: DashboardModuleViewController){
        self.wireframe = wireframe
        self.view = view
    }
    
    func fetchWallet(successHandler: @escaping (()->Void)){
        self.interactor.remote.FetchWallet { (reply, replyCode) in
            switch replyCode{
            case .fetchSuccess:
                guard let data = reply["data"] as? [String:Any] else { return }
                Wallet(json: data).rewrite()
                successHandler()
            default: break
            }
        }
    }
    
    func fetchUserInfo(successHandler: @escaping (()->Void)){
        self.interactor.remote.FetchUser { (reply, replyCode) in
            switch replyCode{
            case .fetchSuccess:
                User(json: reply).rewrite()
                successHandler()
            default: break
            }
        }
    }
    
    func fetchRemoteTransactionHistoryList(in page: Int, successHandler: @escaping (()->Void)){
        self.interactor.remote.getAllTransactionHistories(successHandler: { (json, statusCode) in
            switch statusCode{
            case .noContent: break
            case .fetchSuccess:
                self.transactionHistoryHolder.removeAll()
                guard let transactions = json["data"] as? [[String: Any]] else {
                    return
                }
                
                for transaction in transactions {
                    let holder = transaction as [String: Any]
                    self.transactionHistoryHolder.append(holder)
                }
                successHandler()
                
            default: break
            }
        })
    }
    
}
