//
//  DashboardRemoteInteractor.swift
//  GBA
//
//  Created by Republisys on 24/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

fileprivate enum APICalls: Router{
    var baseComponent: String { get { return "/public" } }
    
    case fetchWallet
    case fetchUser
    case getAllTransactionHistories
    
    var route: Route{
        switch self{
        case .fetchWallet:
            return Route(method: .get,
                         suffix: "/wallets",
                         parameters: nil,
                         waitUntilFinished: true,
                         nonToken: false)
        case .fetchUser:
            return Route(method: .get,
                         suffix: "/profile",
                         parameters: nil,
                         waitUntilFinished: true,
                         nonToken: false)
        case .getAllTransactionHistories:
            return Route(method: .get,
                         suffix: "/transactions",
                         parameters: nil,
                         waitUntilFinished: true,
                         nonToken: false)
        }
    }
}

class DashboardRemoteInteractor: RootRemoteInteractor{
    func FetchWallet(successHandler: @escaping (JSON, ServerReplyCode)->Void){
        NetworkingManager.request(APICalls.fetchWallet, successHandler: { (reply, replyCode) in
            successHandler(reply, replyCode)
        })
    }
    
    func FetchUser(successHandler: @escaping (JSON, ServerReplyCode)->Void){
        NetworkingManager.request(APICalls.fetchUser, successHandler: { (reply, replyCode) in
            successHandler(reply, replyCode)
        })
    }
    
    func getAllTransactionHistories(successHandler: @escaping (JSON, ServerReplyCode)->Void){
        NetworkingManager.request(APICalls.getAllTransactionHistories, successHandler: { (reply, replyCode) in
            successHandler(reply, replyCode)
        })
    }
}
