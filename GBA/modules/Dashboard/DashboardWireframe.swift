//
//  DashboardWireframe.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/20/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit


class DashboardWireframe: RootWireframe{
    
    
    func configureModule(activity: DashboardActivities){
        guard let target =  self._target else{
            fatalError("[EntryWireframe: configureModule] target not found")
        }
        
        if _presenter == nil{
            
            guard let presenter = activity.getPresenter(with: _target?.module.activity?.viewController as! DashboardModuleViewController, and: self) as? DashboardRootPresenter else{
                fatalError("declared presenter cannot be found in Entry module")
            }
            
            self.set(presenter: presenter)
        }else{
            guard let presenter = _presenter as? DashboardRootPresenter else{
                fatalError("declared presenter cannot be parsed for Entry module")
            }
            
            presenter.view = target.module.activity!.viewController as! DashboardModuleViewController
            self.set(presenter: presenter)
        }
    }
    
    func show(`from` activity: DashboardActivities, with transition: Transition, animated: Bool = true){
        self.set(target: .Dashboard(activity))
        self.configureModule(activity: activity)
        self.show(with: transition, animated: animated)
    }
}

extension DashboardWireframe{
    
    func navigate(to activity: DashboardActivities, with presenter: RootPresenter? = nil){
        var transition: Transition = .present
        
        self.set(presenter: presenter)
        
        switch activity {
        case .Dashboardscreen:
            transition = .root
        }
        self.show(from: activity, with: transition, animated: true)
    }
}


extension DashboardWireframe{
    
    func presentTabBarController(){
        self.set(target: .Dashboard(.Dashboardscreen))
        guard let vc = _target?.module.board.instantiateViewController(withIdentifier: "dashboardTabbarController") else { fatalError("viewController not properly fetched") }
        
        self.navigator.setViewControllers([vc], animated: true)
    }
    
    func getViewController(from activity: DashboardActivities)->DashboardModuleViewController?{
        self.set(target: .Dashboard(activity))
        self.configureModule(activity: activity)
        
        guard let module = _target?.module,
            let viewController = module.activity?.viewController as? DashboardModuleViewController,
            let presenter = self._presenter else { return nil}
        
        viewController.set(presenter: presenter)
        
        return viewController
    }
}


