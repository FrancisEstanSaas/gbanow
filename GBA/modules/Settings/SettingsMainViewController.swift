//
//  SettingsMainViewController.swift
//  GBA
//
//  Created by Gladys Prado on 6/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class SettingsMainViewController: SettingsRootViewController{
    
    @IBOutlet weak var btnMyCard: UIButton!
    @IBOutlet weak var btnAccessSettings: UIButton!
    @IBOutlet weak var imgMyCardDisclosure: UIImageView!
    @IBOutlet weak var imgAccessSettingsDisclosure: UIImageView!
    
    var currentPresenter: SettingsMenuPresenter{
        guard let prsntr = self.presenter as? SettingsMenuPresenter
            else{ fatalError("Error in parsing presenter for RegistrationViewController") }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let nav = self.navigationController else {
            fatalError("NavigationController was not set for SettingsMainViewController")
        }
        
        nav.isNavigationBarHidden = false
        
        self.imgMyCardDisclosure.image = UIImage.fontAwesomeIcon(name: .angleRight,
                                                           textColor: GBAColor.gray.rawValue,
                                                           size: CGSize(width: 40,
                                                                        height: 40))
        self.imgAccessSettingsDisclosure.image = UIImage.fontAwesomeIcon(name: .angleRight,
                                                                 textColor: GBAColor.gray.rawValue,
                                                                 size: CGSize(width: 40,
                                                                              height: 40))

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.title = "Settings"
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.title = " "
    }
    
    @objc override func backBtn_tapped(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnMyCardTapped(_ sender: Any) {
        self.presenter.wireframe.navigate(to: .MyCardView)
    }
    
    @IBAction func btnAccessSettingsTapped(_ sender: Any) {
        //self.presenter.wireframe.navigate(to: .PinVerification)
    }
}

extension SettingsMainViewController: PinCodeVerificationDelegate{
    
    func PinCodeVerification() {
        self.presenter.wireframe.navigate(to: .AccessSettingsMainView)
    }
}
