//
//  SettingsWireframe.swift
//  GBA
//
//  Created by Gladys Prado on 6/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class SettingsWireframe: RootWireframe {
    
    func configureModule(activity: SettingsActivities) {
        guard let target = self._target else {
            fatalError("[SettingsWireframe: configureModule] target not found")
        }
        
        if _presenter == nil {
            
            guard let presenter = activity.getPresenter(with: _target?.module.activity?.viewController as! SettingsRootViewController, and: self) as? SettingsRootPresenter else {
                fatalError("declared parameter cannot be found in Settings module")
            }
            
            self.set(presenter: presenter)
        } else {
            guard let presenter = _presenter as? SettingsRootPresenter else {
                fatalError("declared presenter cannot be parsed for Settings module")
            }
            
            presenter.view = target.module.activity!.viewController as! SettingsRootViewController
            self.set(presenter: presenter)
        }
    }
    
    func show(`from` activity: SettingsActivities, with transition: Transition, animated: Bool = true){
        self.set(target: .Settings(activity))
        self.configureModule(activity: activity)
        self.show(with: transition, animated: animated)
    }
}

extension SettingsWireframe{
    func navigate(to activity: SettingsActivities, with presenter: RootPresenter? = nil){
        var transition: Transition = .present
        
        self.set(presenter: presenter)
        
        switch activity {
        case .SettingsMenuView:
            transition = .root
        case .MyCardView:
            transition = .push
        case .AccessSettingsMainView:
            transition = .push
        case .UpdatePasswordView:
            transition = .push
        case .ProfileView:
            transition = .push
        case .EditProfileView:
            transition = .push
//            if presenter == nil { fatalError("no presenter found to initialize in editPayeescreen") }
        }
        self.show(from: activity, with: transition, animated: true)
    }
    
    func getViewController(from activity: SettingsActivities)->SettingsRootViewController?{
        self.set(target: .Settings(activity))
        self.configureModule(activity: activity)
        
        guard let module = _target?.module,
            let viewController = module.activity?.viewController as? SettingsRootViewController,
            let presenter = self._presenter else { return nil }
        
        viewController.set(presenter: presenter)
        
        return viewController
    }
}

extension SettingsWireframe{
    
    func presentCodeVerificationViewController(from sender: GBAVerificationCodeDelegate, completion: (()->Void)?, apiCalls: @escaping ((String, GBACodeVerificationViewController)->Void), backAction: @escaping (()->Void)){
        let vc = GBACodeVerificationViewController()
            .set(delegate: sender)
            .setApiCall(apiCalls)
            .setBackButtonAction(backAction)
        
        if completion != nil { vc.setAction(completion!) }
        
        self.navigator.pushViewController(vc, animated: true)
        
    }
    
    func presentSuccessPage(title: String, message: NSAttributedString, doneAction: (()->Void)? = nil){
        let vc = GBASuccessPage()
            .set(title: title)
            .set(message: message)
            .set(doneAction: doneAction)
        
        self.navigator.pushViewController(vc, animated: true)
    }
    
    func presentPinCodeVC(){
        let vc = PinCodeVerificationController()
        vc.navigationController?.navigationBar.isHidden = false
        self.navigator.pushViewController(vc, animated: true)
    }
    
    func changePinCodeVC(){
        let vc = ChangePinCodeViewController()
        self.navigator.pushViewController(vc, animated: true)
    }
    
    func repeatPinCodeVC(){
        let vc = PinCodeRepeaterViewController()
        self.navigator.pushViewController(vc, animated: true)
    }
    
    func oldPinCodeVC(){
        let vc = OldPinCodeVerifierViewController()
        self.navigator.pushViewController(vc, animated: true)
    }
    
    func settingsPinVC(){
        let vc = PinSettingsViewController()
        self.navigator.pushViewController(vc, animated: true)
    }
    
}


