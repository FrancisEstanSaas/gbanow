//
//  SettingsLocalInteractor.swift
//  GBA
//
//  Created by Gladys Prado on 8/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

class SettingsLocalInteractor: RootLocalInteractor{
    var wallet:Wallet?{ get{ return GBARealm.objects(Wallet.self).first } }
    
    var userProfile: User?{ get { return GBARealm.objects(User.self).first } }
}
