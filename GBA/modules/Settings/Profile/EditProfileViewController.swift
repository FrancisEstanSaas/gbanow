//
//  EditProfileViewController.swift
//  GBA
//
//  Created by Gladys Prado on 18/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import Alamofire
import CryptoSwift

class EditProfileViewController: SettingsRootViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    @IBOutlet weak var profile_imageView: UIImageView!
    @IBOutlet weak var nickname_label: UILabel!
    
    @IBOutlet weak var firstName_textField: GBATitledTextField!
    @IBOutlet weak var lastName_textField: GBATitledTextField!
    
    @IBOutlet weak var mobileNumber_textField: GBATitledTextField!
    @IBOutlet weak var email_textField: GBATitledTextField!
    @IBOutlet weak var country_dropDown: GBAPopOverButton!
    
    private var selectedCountry: Countries = .Philippines
    
    //EXP.
    fileprivate var profile: User?{
        get{
            return self.presenter.interactor.local.userProfile
        }
    }
    
    //for UploadPhoto
    var typeOfImage: String = ""
    var img = UIImageView()
    
    
    var editProfileForm: EditProfile{
        return EditProfile(firstName: self.firstName_textField.text, lastName: self.lastName_textField.text)
    }
    
    var currentPresenter: EditProfilePresenter{
        guard let prsntr = self.presenter as? EditProfilePresenter
            else{ fatalError("Error in parsing presenter for RegistrationViewController") }
        return prsntr
    }
        
    override func viewDidLoad() {
        print("*** ViewDidLoad ***")
        
        self.presenter.set(view: self)
        (self._presenter as! EditProfilePresenter).dataBridge = self
        
        let defaultCountry = Countries.Philippines
        
        let rightBarButton = UIBarButtonItem(title: String.fontAwesomeIcon(name: .ellipsisH), style: .plain, target: self, action: #selector(showMenu))
        rightBarButton.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)], for: .normal)
        let backBtn = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        backBtn.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)], for: .normal)
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.rightBarButtonItem = rightBarButton
        
        self.firstName_textField.isUserInteractionEnabled = false
        self.lastName_textField.isUserInteractionEnabled = false
        self.profile_imageView.isUserInteractionEnabled = false
        
        //profile_imageView
        self.profile_imageView.layer.cornerRadius = profile_imageView.frame.size.width/2
        self.profile_imageView.clipsToBounds = true
        self.profile_imageView.layer.borderWidth = 3
        self.profile_imageView.layer.borderColor = UIColor.white.cgColor
        
        firstName_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "First name")
            .set(next: lastName_textField)
        
        lastName_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .done)
            .set(placeholder: "Last name")
        
        country_dropDown
            .set(parent: self)
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
            .set(text: defaultCountry.name)
            .set(placeholder: "Country")
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))
        
        mobileNumber_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Mobile")
            .set(next: email_textField)
        
        email_textField
            .set(self)
            .set(alignment: .left)
            .set(returnKey: .done)
            .set(placeholder: "Email")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("*** ViewDidAppear ***")
        super.viewDidAppear(animated)
        self.title = "Profile"
        self.repopulateProfileInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("*** ViewWillAppear ***")
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("*** ViewWillDisappear ***")
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    
    @IBAction func selectImageForProfilePicture(_ sender: UITapGestureRecognizer) {
        self.bottomActionSheetPressed()
        print("*** selectImage Tapped ***")
    }
    
    
    @objc private func showMenu(){
        let menu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let editPayee = UIAlertAction(title: "Edit Profile", style: .default, handler: { (alert:UIAlertAction!) -> Void in self.editProfileInfo()})
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        menu.addAction(editPayee)
        menu.addAction(cancel)
        self.present(menu, animated: true, completion: nil)
    }
    
    @objc override func backBtn_tapped() {
        guard let nav = self.navigationController else { fatalError("NavigationViewController can't properly parsed")}
        nav.dismiss(animated: true, completion: nil)
        //self.presenter.wireframe.popFromViewController(true)
    }
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        //self.view.subviews.forEach{ $0.resignFirstResponder() }

        if (self.firstName_textField.text != "" && self.lastName_textField.text != "") {
            self.submitAlertAction()
            //self.showProfileChangesAlert()
            //self.currentPresenter.wireframe.dismiss(true)
        //self.navigationController?.dismiss(animated: true, completion: nil)
            //self.currentPresenter.wireframe.navigate(to: .ProfileView)
        } else {
            testRequiredFields() }
        }
    
    @objc func backBtnAlert_tapped() {
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel editing our Profile?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil) }
    
    func decryptUserData(key: String, iv: String, userData: String) -> String {
        let data = Data(base64Encoded: userData)!
        let decrypted = try! AES(key: key, iv: iv).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
    }
    
    
    private func repopulateProfileInfo(){
        guard let userProfile = self.profile else {
            return
        }
        self.firstName_textField
            .set(text: self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.firstname))
        self.lastName_textField
            .set(text: self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.lastname))
        self.nickname_label.text = "\(self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.firstname))"
        self.mobileNumber_textField
            .set(text: self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.mobile))
        self.email_textField
            .set(text: self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.email))
    }
    
    //test validations
    func testRequiredFields(){
        
        self.firstName_textField
            .set(required: true)
            .set(validation: .name(message: "Invalid first name"))
        
        self.lastName_textField
            .set(required: true)
            .set(validation: .name(message: "Invalid last name"))
    }
    
    func showProfileChangesAlert() {
        let alertView = UIAlertController(title: "Profile Updated",
                                          message: "Your Profile is now updated",
                                          preferredStyle:. alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.transactionSubmitted()}))
        present(alertView, animated: true)
    }
    
    //MARK: PhotoLibrary Functions
    func selectFromGallery() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.sourceType = .photoLibrary
            imagePickerController.delegate = self
            present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: Camera Functions
    func getAnImage() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraImagePickerController = UIImagePickerController()
            cameraImagePickerController.sourceType = .camera
            cameraImagePickerController.delegate = self
            present(cameraImagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK: Bottom ActionSheet
    
    func bottomActionSheetPressed() {
        let alert = UIAlertController(title: nil, message: "Select image for your profile photo", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Select from gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.selectFromGallery()}))
        alert.addAction(UIAlertAction(title: "Capture an image", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.getAnImage()}))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    // Alert for the submitAlertAction()
    func submitAlertAction() {
        let alert = UIAlertController(title: "Update Profile", message: "Do you like the following changes be saved?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.showProfileChangesAlert()}))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: PhotoLibrary Function Delelegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {fatalError("Expected a dictionary containing an image, but was provided the following: \(info)") }
        img = UIImageView(image: selectedImage)
        let imageData = UIImageJPEGRepresentation(selectedImage, 0.5)
        //self.currentPresenter.uploadProfilePicture(image: imageData!)
        switch typeOfImage {
            case "Camera":
                self.profile_imageView.image = selectedImage
                //self.uploadWithAlamofire(profileImage: selectedImage)
                self.currentPresenter.uploadProfilePicture(image: imageData!)
            //self.savePhoto()
            
            
            default:
                self.profile_imageView.image = selectedImage
                //self.uploadWithAlamofire(profileImage: selectedImage)
                self.currentPresenter.uploadProfilePicture(image: imageData!)
                //self.savePhoto()
            break
        }
        self.profile_imageView.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    

    
    // Segue for the Submit Button
    func transactionSubmitted() {
        self.currentPresenter.processEditProfileInfo(submittedForm: editProfileForm)
        let backBtn = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        backBtn.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)], for: .normal)
        self.navigationItem.leftBarButtonItem = backBtn
        self.firstName_textField.isUserInteractionEnabled = false
        self.lastName_textField.isUserInteractionEnabled = false
        self.repopulateProfileInfo()
    
    }
    
    
    func editProfileInfo() {
        self.title = "Edit Profile"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        let backBtn = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtnAlert_tapped))
        backBtn.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)], for: .normal)
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        self.firstName_textField.isUserInteractionEnabled = true
        self.lastName_textField.isUserInteractionEnabled = true
        self.profile_imageView.isUserInteractionEnabled = true
        self.firstName_textField.becomeFirstResponder()
    }


}

extension EditProfileViewController: GBAFocusableInputViewDelegate{
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
    
    func didChange(value: String?) {
        self.nickname_label.text = value
    }
    
//    // import Alamofire
//    func uploadWithAlamofire(profileImage: UIImage) {
//        
//        let parameters = ["is_profile" : "1"]
//        let imageData = UIImageJPEGRepresentation(profileImage, 0.5)
//        
//        Alamofire.upload(
//            multipartFormData: { multipartFormData in
//                multipartFormData.append(imageData!, withName: String(describing: profileImage))
//                for (key,value) in parameters {
//                    multipartFormData.append((value ).data(using: .utf8)!, withName: key )
//                }
//        },
//            to: "http://10.10.10.30:80/gba-webservice-1.0/images",
//            method: .post,
//            encodingCompletion: { encodingResult in
//                switch encodingResult {
//                case .success(let upload, _, _):
//                    
//                    upload.uploadProgress(closure: { (Progress) in
//                        print("Upload Progress: \(Progress.fractionCompleted)")
//                    })
//                    
//                    upload.responseJSON { response in
//                        debugPrint("SUCCESS ", response)
//                        
//                    }
//                case .failure(let error):
//                    print("\n\n===========Error===========")
//                    print("Error Code: \(error._code)")
//                    print("Error Messsage: \(error.localizedDescription)")
//                    debugPrint(error as Any)
//                    print("===========================\n\n")
//                }
//        }
//        )
//    }
    
}



extension EditProfileViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension EditProfileViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBACountryCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        
        country_dropDown.set(icon: cell.flagImage.image!)
        country_dropDown.set(text: cell.countryLabel.text!)
        self.selectedCountry = cell.country
    }
}

extension EditProfileViewController: DataDidReceiveFromEditProfile{
    func didReceiveResponse(code: String) {
        print("Data Received")
    }
}


