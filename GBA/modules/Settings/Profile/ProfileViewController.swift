//
//  ProfileViewController.swift
//  GBA
//
//  Created by Gladys Prado on 18/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift

class ProfileViewController: SettingsRootViewController {
    
    @IBOutlet weak var profile_imageView: UIImageView!
    @IBOutlet weak var nickname_label: UILabel!
    
    @IBOutlet weak var firstName_textField: GBATitledTextField!
    @IBOutlet weak var lastName_textField: GBATitledTextField!
    @IBOutlet weak var mobileNumber_textField: GBATitledTextField!
    @IBOutlet weak var email_textField: GBATitledTextField!
    @IBOutlet weak var country_dropDown: GBAPopOverButton!
    
    //EXP.
    fileprivate var profile: User?{
        get{
            return self.presenter.interactor.local.userProfile
        }
    }
    
    //EXP.
    fileprivate var user: User{
        get{
            guard let usr = GBARealm.objects(User.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }
    
    private var selectedCountry: Countries = .Philippines
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        let defaultCountry = Countries.Philippines
        
        let rightBarButton = UIBarButtonItem(title: String.fontAwesomeIcon(name: .ellipsisH), style: .plain, target: self, action: #selector(showMenu))
        rightBarButton.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)], for: .normal)
        let backBtn = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        backBtn.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)], for: .normal)
        self.navigationItem.leftBarButtonItem = backBtn
        self.navigationItem.rightBarButtonItem = rightBarButton

        profile_imageView
            .roundCorners()
        
        firstName_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "First name")
            .set(next: lastName_textField)
            .set(textFieldIsEditable: false)
        
        lastName_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Last name")
            .set(next: mobileNumber_textField)
            .set(textFieldIsEditable: false)
        
        country_dropDown
            .set(parent: self)
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
            .set(text: defaultCountry.name)
            .set(placeholder: "Country")
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))
        
        mobileNumber_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(returnKey: .next)
            .set(placeholder: "Mobile")
            .set(next: email_textField)
            .set(textFieldIsEditable: false)
        
        email_textField
            .set(self)
            .set(max: 1)
            .set(alignment: .left)
            .set(returnKey: .done)
            .set(placeholder: "Email")
            .set(textFieldIsEditable: false)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Profile"
        self.repopulateProfileInfo()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    func decryptUserData(key: String, iv: String, userData: String) -> String {
        let data = Data(base64Encoded: userData)!
        let decrypted = try! AES(key: key, iv: iv).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
    }
    
    private func repopulateProfileInfo(){
        guard let userProfile = self.profile else {
            return
        }
        self.firstName_textField
            .set(text: self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.firstname))
        self.lastName_textField
            .set(text: self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.lastname))
        self.nickname_label.text = "\(self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.firstname))"
        self.mobileNumber_textField
            .set(text: self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.mobile))
        self.email_textField
            .set(text: self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.email))
    }
    
    @objc private func showMenu(){
        let menu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let editPayee = UIAlertAction(title: "Edit Profile", style: .default, handler: { (alert:UIAlertAction!) -> Void in self.presenter.wireframe.navigate(to: .EditProfileView)})
        let cancel = UIAlertAction(title: "Cancel", style: .cancel)
        menu.addAction(editPayee)
        menu.addAction(cancel)
        self.present(menu, animated: true, completion: nil)
    }
    
    @objc override func backBtn_tapped() {
        guard let nav = self.navigationController else { fatalError("NavigationViewController can't properly parsed")}
        nav.dismiss(animated: true, completion: nil)
        //self.presenter.wireframe.popFromViewController(true)
    }

}

extension ProfileViewController: GBAFocusableInputViewDelegate{
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
    
    
}

extension ProfileViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension ProfileViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBACountryCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        
        country_dropDown.set(icon: cell.flagImage.image!)
        country_dropDown.set(text: cell.countryLabel.text!)
        self.selectedCountry = cell.country
    }
}
