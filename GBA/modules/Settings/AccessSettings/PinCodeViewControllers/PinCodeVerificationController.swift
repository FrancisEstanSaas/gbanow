//
//  PinCodeVerificationController.swift
//  GBA
//
//  Created by EDI on 23/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import CryptoSwift
import KeychainAccess

protocol PinCodeVerificationDelegate{
    func PinCodeVerification()
}

class PinCodeVerificationController: SettingsRootViewController, UITextFieldDelegate{
    
    //Realm Q.Variables
    fileprivate var user: User{
        get{
            guard let usr = GBARealm.objects(User.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }
    
    fileprivate var primaryUser: UserKeyInfo{
        get{
            guard let usrP = GBARealm.objects(UserKeyInfo.self).first else{
                fatalError("User not found")
            }
            return usrP
        }
    }
    
    fileprivate var userPinCode: String = ""
    
    private let header_label: UILabel = UILabel()
        .set(fontStyle: GBAText.Font.main(GBAText.Size.header.rawValue).rawValue)
        .set(value: "Enter PIN code")
        .set(color: GBAColor.black.rawValue)
        .set(alignment: .center)
        .set(lines: 1)
    
    private let subscript_label: UILabel = UILabel()
        .set(fontStyle: GBAText.Font.main(GBAText.Size.subContent.rawValue).rawValue)
        .set(value: "Please enter your 6-digit PIN to access Settings")
        .set(color: GBAColor.gray.rawValue)
        .set(alignment: .center)
        .set(lines: 0)
    
    private let toggleSetView: UIView = UIView()
    private let toggleViews: [GBAToggleView] = [GBAToggleView(), GBAToggleView(), GBAToggleView(), GBAToggleView(), GBAToggleView(), GBAToggleView()]
    
    
    private let input_textField: UITextField = UITextField()
    
    private var pinCodeInput: NSString = ""{
        didSet{
            self.toggleViews.forEach{ $0.toggle = .off }
            for i in 0..<pinCodeInput.length{
                self.toggleViews[i].toggle = .on
            }
            self.toggleSetView.layoutIfNeeded()
            if self.pinCodeInput.length == 6{
                self.pinCodeValidator(pin: pinCodeInput)
            }
        }
    }
    
    init(){
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barStyle = .black
        self.view.backgroundColor = .white
        self.title = "PIN Code"
        self.navigationController?.isNavigationBarHidden = false
        self.layoutContents()
        self.newPinUserValidator()
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.input_textField.becomeFirstResponder()
    }
    
    private func layoutContents(){
        
        toggleSetView.backgroundColor = .clear
        
        self.input_textField.keyboardType = .numberPad
        self.input_textField.delegate = self
        
        self.view.addSubview(header_label)
        self.view.addSubview(subscript_label)
        self.view.addSubview(toggleSetView)
        self.view.addSubview(input_textField)
        
        self.header_label.translatesAutoresizingMaskIntoConstraints = false
        self.header_label.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor, constant: 30).Enable()
        self.header_label.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).Enable()
        self.header_label.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).Enable()
        
        self.subscript_label.translatesAutoresizingMaskIntoConstraints = false
        self.subscript_label.topAnchor.constraint(equalTo: self.header_label.bottomAnchor, constant: 5).Enable()
        self.subscript_label.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).Enable()
        self.subscript_label.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).Enable()
        
        self.toggleSetView.translatesAutoresizingMaskIntoConstraints = false
        self.toggleSetView.topAnchor.constraint(equalTo: self.subscript_label.bottomAnchor, constant: 50).Enable()
        self.toggleSetView.leadingAnchor.constraint(equalTo: self.subscript_label.leadingAnchor, constant: 35).Enable()
        self.toggleSetView.trailingAnchor.constraint(equalTo: self.subscript_label.trailingAnchor, constant: -35).Enable()
        self.toggleSetView.heightAnchor.constraint(equalTo: self.toggleSetView.widthAnchor, multiplier: 0.1).Enable()
        
        self.view.layoutIfNeeded()
        self.layoutToggleViews()
    }
    
    private func layoutToggleViews(){
        
        let spacing = (self.toggleSetView.width - (self.toggleSetView.height * 6)) / 5
        
        for i in 0..<toggleViews.count{
            toggleViews[i].toggle = .off
            self.toggleSetView.addSubview(self.toggleViews[i])
            
            self.toggleViews[i].translatesAutoresizingMaskIntoConstraints = false
            self.toggleViews[i].topAnchor.constraint(equalTo: self.toggleSetView.topAnchor).Enable()
            self.toggleViews[i].bottomAnchor.constraint(equalTo: self.toggleSetView.bottomAnchor).Enable()
            self.toggleViews[i].widthAnchor.constraint(equalTo: self.toggleViews[i].heightAnchor).Enable()
            
            if i == 0{ self.toggleViews[i].leadingAnchor.constraint(equalTo: self.toggleSetView.leadingAnchor).Enable() }
            else { self.toggleViews[i].leadingAnchor.constraint(equalTo: self.toggleViews[i - 1].trailingAnchor, constant: spacing).Enable() }
            
            if i == toggleViews.count - 1{ self.toggleViews[i].trailingAnchor.constraint(equalTo: self.toggleSetView.trailingAnchor).Enable() }
            toggleViews[i].layoutIfNeeded()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if pinCodeInput.length >= 6{ return false }
        var text = "\(String(describing: textField.text!))\(string)"
        if string == "" { text.removeLast() }
        pinCodeInput = text as NSString
        return true
    }
    
    //CryptoSwift func
    func encryptUserData(key: String, iv: String, userData: String) -> String {
        let data = userData.data(using: .utf8)!
        let encrypted = try! AES(key: key, iv: iv).encrypt([UInt8](data))
        let encryptedData = Data(encrypted)
        return encryptedData.base64EncodedString()
    }
    
    func decryptUserData(key: String, iv: String, userData: String) -> String {
        let data = Data(base64Encoded: userData)!
        let decrypted = try! AES(key: key, iv: iv).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
    }
    
    //Exp. Pin Validator
    func newPinUserValidator() {
        let pinUserProfile = self.primaryUser
        if pinUserProfile.userPIN == nil {
            self.header_label.set(value: "Set New PIN Code")
            self.subscript_label.set(value: "Enter 6 digits to set as a New PIN Code")
        } else {
            print("PinUser already set a PINcode")
            return
        }
        
    }
    
    //For Existing PinUsers
    func existingPinUserValidator(enteredPinCode: String) {
        let existingPIN = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: primaryUser.userPIN!)
        if enteredPinCode == existingPIN {
            guard let navController = self.navigationController else { return }
            SettingsWireframe(navController).navigate(to: .AccessSettingsMainView)
        } else {
            self.subscript_label.set(value: "Entered PIN Code is incorrect, please try again")
            self.clearPIN()
            //self.input_textField.reloadInputViews()
        }
    }
    
    //PIN Validator
    func pinCodeValidator(pin: NSString) {
        let pinUserProfile = self.primaryUser
        let enteredPin = String(pin)
        
        if pinUserProfile.userPIN == nil {
            self.newUserPinProcessor(enteredPinCode: enteredPin)
        } else {
            // For Existing PinUsers
            self.existingPinUserValidator(enteredPinCode: enteredPin)
        }
    }
    
    //EXPerimentor
    func newUserPinProcessor(enteredPinCode: String) {
        let userProfile = self.primaryUser
        let newUser = UserKeyInfo()
        newUser.userNumber = userProfile.userNumber
        newUser.userPassword = userProfile.userPassword
        newUser.userPIN = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: enteredPinCode)
        
        //Realm
        do {
            try GBARealm.write {
                GBARealm.add(newUser, update: true)
            }
        } catch {
            print(error.localizedDescription)
        }
        
        guard let navController = self.navigationController else { return }
        SettingsWireframe(navController).repeatPinCodeVC()
        
    }
    
    
    func clearPIN(){
        self.pinCodeInput = ""
        self.input_textField.text = ""
        self.toggleViews.forEach{ $0.toggle = .off }
    }
    
}


