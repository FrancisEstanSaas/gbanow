//
//  ReloadRootPresenter.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class ReloadRootPresenter: RootPresenter{
    var wireframe: ReloadWireframe
    
    var interactor: (remote: ReloadRemoteInteractor, local: ReloadLocalInteractor) = (ReloadRemoteInteractor(),ReloadLocalInteractor())
    var view: ReloadRootViewController
    
    init(wireframe: ReloadWireframe, view: ReloadRootViewController) {
        self.wireframe = wireframe
        self.view = view
    }
    
    func set(view: ReloadRootViewController){
        self.view = view
    }
    
    func showAlert(with title: String?, message: String, completion: @escaping (()->Void)){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.view.present(alert, animated: true, completion: nil)
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { _ in
            alert.dismiss(animated: true, completion: completion)
        }
    }
}


