//
//  ReloadOptionsViewController.swift
//  GBA
//
//  Created by Gladys Prado on 27/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class ReloadOptionsViewController: ReloadRootViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblReloadOptions: UITableView!
    
    var reloadOptions = ["Direct Debit Account Deposit" : "Amount_Blue", "Credit Card" : "Source_Account_Blue"]
    
    //set cell indentifier
    var cellIdentifier = "ReloadOption"
    
    override func viewDidLoad() {
        self.addBackButton()
        
        tblReloadOptions.delegate = self
        tblReloadOptions.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Reload Wallet"
    
    }
    
    @objc override func backBtn_tapped(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    //UITableViewController Setup
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? ReloadOptionTableViewCell else {
            fatalError("The dequeued cell is not an instance of ReloadOptionTableViewCell.")
        }
        
        let reloadOptionName = Array(reloadOptions)[indexPath.row].key
        let reloadOptionImage = Array(reloadOptions)[indexPath.row].value
        
        cell.optionImage?.image = UIImage(named: reloadOptionImage)
        cell.optionName?.text = reloadOptionName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        indexPath.row == 0 ?
            (self.presenter.wireframe.navigate(to: .DirectDebitAccountDepositView)):
            (self.presenter.wireframe.navigate(to: .CreditCardDepositView))
        
//        self.presenter.wireframe.navigate(to: .DirectDebitAccountDepositView)
    }
    
}
