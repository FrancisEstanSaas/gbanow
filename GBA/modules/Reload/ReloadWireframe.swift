//
//  ReloadWireframe.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

class ReloadWireframe: RootWireframe {
    
    func configureModule(activity: ReloadActivities) {
        guard let target = self._target else {
            fatalError("[ReloadWireframe: configureModule] target not found")
        }
        
        if _presenter == nil {
            
            guard let presenter = activity.getPresenter(with: _target?.module.activity?.viewController as! ReloadRootViewController, and: self) as? ReloadRootPresenter else {
                fatalError("declared parameter cannot be found in Reload module")
            }
            
            self.set(presenter: presenter)
        } else {
            guard let presenter = _presenter as? ReloadRootPresenter else {
                fatalError("declared presenter cannot be parsed for Reload module")
            }
            
            presenter.view = target.module.activity!.viewController as! ReloadRootViewController
            self.set(presenter: presenter)
        }
    }
    
    func show(`from` activity: ReloadActivities, with transition: Transition, animated: Bool = true){
        self.set(target: .Reload(activity))
        self.configureModule(activity: activity)
        self.show(with: transition, animated: animated)
    }
}

extension ReloadWireframe{
    func navigate(to activity: ReloadActivities, with presenter: RootPresenter? = nil){
        var transition: Transition = .present
        
        self.set(presenter: presenter)
        
        switch activity {
        case .ReloadOptionsView:
            transition = .root
        case .DirectDebitAccountDepositView:
            transition = .push
        case .ReloadPhotoUploadView:
            transition = .push
        case .ReloadAmountDepositedView:
            transition = .push
        case .ReloadAccountDestinationView:
            transition = .push
        case .ReloadTransactionSummaryView:
            transition = .push
        case .CreditCardDepositView:
            transition = .push
        }
        self.show(from: activity, with: transition, animated: true)
    }
    
    func getViewController(from activity: ReloadActivities)->ReloadRootViewController?{
        self.set(target: .Reload(activity))
        self.configureModule(activity: activity)
        
        guard let module = _target?.module,
            let viewController = module.activity?.viewController as? ReloadRootViewController,
            let presenter = self._presenter else { return nil }
        
        viewController.set(presenter: presenter)
        
        return viewController
    }
}

extension ReloadWireframe{
    func presentSuccessPage(title: String, message: NSAttributedString, doneAction: (()->Void)? = nil){
        let vc = GBASuccessPage()
            .set(title: title)
            .set(message: message)
            .set(doneAction: doneAction)
        
        self.navigator.pushViewController(vc, animated: true)
    }
}
