//
//  CreditCardDepositViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation
import UIKit

class CreditCardDepositViewController: ReloadRootViewController, GBAFocusableInputViewDelegate {
    
    @IBOutlet weak var txtCardName: GBATitledTextField!
    @IBOutlet weak var txtCardNumber: GBATitledTextField!
    @IBOutlet weak var txtCardExpirationDate: GBATitledTextField!
    @IBOutlet weak var txtCardCVV: GBATitledTextField!
    @IBOutlet weak var txtCardDepositAmount: GBATitledTextField!
    
    @IBOutlet var requiredFields: [GBATitledTextField]!

    fileprivate var wallet: Wallet?{
        get{ return self.presenter.interactor.local.wallet }
    }
    
    var directDebitDepositForm: DirectDepositEntity{
        return DirectDepositEntity( wallet_id: "\(self.wallet!.id)",
            amount: self.txtCardDepositAmount.text,
            account_name: self.txtCardName.text,
            account_number: self.txtCardNumber.text,
            aba_number: self.txtCardCVV.text)
    }
    
    var currentPresenter: CreditCardDepositPresenter{
        guard let prsntr = self.presenter as? CreditCardDepositPresenter
            else{ fatalError("Error in parsing presenter for DirectDebitAccountDepositViewController") }
        return prsntr
    }
    
    var didEditForm = false
    
    override func viewDidLoad() {
        print("self.presenter ", self.presenter)
        super.viewDidLoad()
        self.presenter.set(view: self)
        
        self.txtCardName
            .set(self)
            .set(placeholder: "Card Name")
            .set(returnKey: .next)
            .set(next: txtCardNumber)
            .set(required: true)
            .set(validation: .name(message: "Invalid card name"))
        
        self.txtCardNumber
            .set(self)
            .set(placeholder: "Card Number")
            .set(keyboardType: .numberPad)
            .set(returnKey: .next)
            .set(next: txtCardExpirationDate)
            .set(required: true)
        
        self.txtCardExpirationDate
            .set(self)
            .set(placeholder: "Expiration (MM/YY)")
            .set(keyboardType: .numberPad)
            .set(inputType: .expiryDate)
            .set(returnKey: .next)
            .set(next: txtCardCVV)
            .set(required: true)
        
        self.txtCardCVV
            .set(self)
            .set(placeholder: "CVV")
            .set(keyboardType: .numberPad)
            .set(inputType: .cardCVV)
            .set(security: true)
            .set(returnKey: .next)
            .set(next: txtCardDepositAmount)
            .set(required: true)
        
        self.txtCardDepositAmount
            .set(self)
            .set(placeholder: "Amount")
            .set(keyboardType: .decimalPad)
            .set(returnKey: .done)
            .set(required: true)
        
        self.addBackButton()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.title = "Reload"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .black
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        self.view.backgroundColor = GBAColor.white.rawValue
        
    }
    
    override func backBtn_tapped() {
        
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel your Transaction?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil)
    }
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(backBtn_tapped))
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        self.view.subviews.forEach{ $0.resignFirstResponder() }
        
        /*if (self.txtCardName.text != "" && self.txtCardNumber.text != "" && self.txtCardCVV.text != "" && self.txtCardDepositAmount.text != "" && isCardExpired(self.txtCardExpirationDate.text) == false) {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.currentPresenter.submitDirectDebitDeposit(directDebitDepositReloadForm: directDebitDepositForm)
        } else {
            self.fieldValidations()
        }*/
        
        var formIsValid = true
        
        self.requiredFields.forEach{
            print($0.isValid())
            if !$0.isValid(){ formIsValid = false; return }
            
        }
        
        if (formIsValid){
            if (isCardExpired(self.txtCardExpirationDate.text) == false){
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                self.currentPresenter.submitDirectDebitDeposit(directDebitDepositReloadForm: directDebitDepositForm)
            }
        }
    }
    
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
    
    func fieldValidations(){
        self.txtCardName
            .set(required: true)
            .set(validation: .name(message: "Invalid account name"))
        
        self.txtCardCVV
            .set(required: true)
        
        self.txtCardExpirationDate
            .set(required: true)
        
        self.txtCardNumber
            .set(required: true)
        
        self.txtCardDepositAmount
            .set(required: true)
        
    }
    
    func isCardExpired(_ expiryDate: String) -> Bool {
        let dateNow = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/YY"

        let currentDateConversion = dateFormatter.string(from: dateNow)
        
        if let _ = dateFormatter.date(from: expiryDate){
            if currentDateConversion.compare(expiryDate) == .orderedDescending {
                print("Card expired")
                
                let unauthorizedAccessAlert = UIAlertController(title: "Notice", message: "Card is already expired.", preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    //dismiss alert
                }
                
                unauthorizedAccessAlert.addAction(confirmAction)
                
                self.present(unauthorizedAccessAlert, animated: true, completion: nil)
                
                return true
            } else {
                return false
            }
        }else{

            let unauthorizedAccessAlert = UIAlertController(title: "Notice", message: "Please enter a valid date.", preferredStyle: .alert)
            
            let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                //dismiss alert
            }
            
            unauthorizedAccessAlert.addAction(confirmAction)
            
            self.present(unauthorizedAccessAlert, animated: true, completion: nil)
            
            return true
        }
    }
}

extension CreditCardDepositViewController: GBATitledTextFieldDelegate{
    func didChange(textField: GBATitledTextField) {
        self.didEditForm = true
    }
}
