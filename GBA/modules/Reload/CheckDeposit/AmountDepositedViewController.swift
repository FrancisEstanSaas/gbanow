//
//  AmountDepositedViewController.swift
//  GBA
//
//  Created by Gladys Prado on 16/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class AmountDepositedViewController: ReloadRootViewController {
    
    @IBOutlet weak var txtAmountDeposited: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(tempNextButton))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.title = "Reload"
        
        self.view.bringSubview(toFront: self.navigationController!.navigationBar)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    @objc func tempNextButton() {
        self.presenter.wireframe.navigate(to: .ReloadAccountDestinationView)
    }
}
