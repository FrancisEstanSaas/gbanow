//
//  PendingTransactionsViewController.swift
//  GBA
//
//  Created by Gladys Prado on 1/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PendingTransactionsViewController: TransfersRootViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblPendingTransactionsList: UITableView!
    @IBOutlet weak var selectTransactionListCategory: UISegmentedControl!
    fileprivate var transactionHistoryHolder:[[String: Any]] = [[String: Any]]()
    
    private var currentPresenter: PendingTransactionsPresenter{
        guard let prsntr = self.presenter as? PendingTransactionsPresenter else{
            fatalError("Error parsing presenter in ManagePayeeListController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblPendingTransactionsList.delegate = self
        tblPendingTransactionsList.dataSource = self
        
        self.addBackButton()
        
        self.selectTransactionListCategory.addTarget(self, action: #selector(transactionListChanged(selectedType:)), for: .valueChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.parent?.title = "Pending Transactions"
        
        //fetch data
        self.currentPresenter.fetchScheduledTransactions(in: 0, successHandler: {
            self.transactionHistoryHolder = self.currentPresenter.scheduledTransactionsHolder
            self.tblPendingTransactionsList.reloadData()
        })
        
        self.selectTransactionListCategory.selectedSegmentIndex = 0
    }
    
    @objc override func backBtn_tapped(){
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    //start table view configuration
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.transactionHistoryHolder.count > 0 ){
            return self.transactionHistoryHolder.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = Bundle.main.loadNibNamed("TransactionHistoryCellView", owner: self, options: nil)?.first as? TransactionHistoryCellView else{
            fatalError("Parsing TransactionHistoryCellView not properly parsed for TransactionHistoryViewController")
        }
        
        if (self.transactionHistoryHolder.count > 0 ){
            let transactionDetails = self.transactionHistoryHolder[indexPath.row] as NSDictionary
            let transactionAmount = "\(String(describing: transactionDetails["currency"]!)) \(String(describing: transactionDetails["amount"]!))"
//            let transactionDate = transactionDetails["date"] as? String
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "hh:mm PM"
            
            cell.transactionTitle.text = transactionDetails["name"] as? String
            cell.transactionDate.text = transactionDetails["date"] as? String
            cell.transactionType.text = transactionDetails["trx_type"] as? String
            cell.transactionAmount.text = transactionAmount
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let transactionDetails = self.transactionHistoryHolder[indexPath.row] as NSDictionary
        let transactionID = transactionDetails["id"] as? Int
        self.currentPresenter.transactionID = transactionID!
        self.currentPresenter.fetchPendingTransactionDetails(transactionID: transactionID!, successHandler: { 
            self.presenter.wireframe.navigate(to: .PendingTransactionsDetailsView, with: self.currentPresenter)
        })
    }
    
    //segmented control
    @IBAction func transactionListChanged(selectedType: UISegmentedControl){
        if (self.selectTransactionListCategory.selectedSegmentIndex == 0){
            self.currentPresenter.fetchScheduledTransactions(in: 0, successHandler: {
                self.transactionHistoryHolder = self.currentPresenter.scheduledTransactionsHolder
                self.tblPendingTransactionsList.reloadData()
            })
        } else {
            self.currentPresenter.fetchCompletedTransactions(in: 0, successHandler: {
                self.transactionHistoryHolder = self.currentPresenter.completedTransactionsHolder
                self.tblPendingTransactionsList.reloadData()
            })
        }
    }
    
}
