//
//  TransfersLocalInteractor.swift
//  GBA
//
//  Created by Gladys Prado on 26/1/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import Foundation

class TransfersLocalInteractor: RootLocalInteractor{
    var PayeeList: [Payee]?{ get{ return self.fetchList(entity: Payee.self) } }
    
    var wallet: [Wallet]?{ get{ return self.fetchList(entity: Wallet.self) } }
}
