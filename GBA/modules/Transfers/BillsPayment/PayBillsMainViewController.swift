//
//  PayBillsMainViewController.swift
//  GBA
//
//  Created by Gladys Prado on 22/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PayBillsMainViewController: TransfersRootViewController {
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.bringSubview(toFront: self.navigationController!.navigationBar)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Pay Bills"
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.parent?.title = " "
        
    }
    
}
