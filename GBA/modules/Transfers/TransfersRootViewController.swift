//
//  TransfersRootViewController.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class TransfersRootViewController: RootViewController {
    
    var presenter: TransfersRootPresenter {
        get{
            let transfersPresenter = self._presenter as! TransfersRootPresenter
            return transfersPresenter
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let cancelButton = UIBarButtonItem(title: " ",
                                           style: .plain,
                                           target: self,
                                           action: #selector(tempCancelButton))
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.leftItemsSupplementBackButton = true
        
    }
    
    @objc func tempCancelButton() {
        guard let nav =  self.navigationController else {
            fatalError("")
        }
        
        TransfersWireframe(nav).navigate(to: .TransfersMainView)
        self.tabBarController?.tabBar.isHidden = false
    }
}
