//
//  TransfersWireframe.swift
//  GBA
//
//  Created by Gladys Prado on 9/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

class TransfersWireframe: RootWireframe {
    func configureModule(activity: TransfersActivities) {
        guard let target = self._target else {
            fatalError("[TransfersWireframe: configureModule] target not found")
        }
        
        if _presenter == nil {
            
            guard let presenter = activity.getPresenter(with: _target?.module.activity?.viewController as! TransfersRootViewController, and: self) as? TransfersRootPresenter else {
                fatalError("declared parameter cannot be found in Transfers module")
            }
            
            self.set(presenter: presenter)
        } else {
            guard let presenter = _presenter as? TransfersRootPresenter else {
                fatalError("declared presenter cannot be parsed for Transfers module")
            }
            
            presenter.view = target.module.activity!.viewController as! TransfersRootViewController
            self.set(presenter: presenter)
        }
        
    }
    
    func show(`from` activity: TransfersActivities, with transition: Transition, animated: Bool = true){
        self.set(target: .Transfers(activity))
        self.configureModule(activity: activity)
        self.show(with: transition, animated: animated)
    }
}

extension TransfersWireframe{
    
    func navigate(to activity: TransfersActivities, with presenter: RootPresenter? = nil){
        var transition: Transition = .present
        
        self.set(presenter: presenter)
        
        switch activity {
        case .TransfersMainView:
            transition = .root
        case .PayBillsMainView:
            transition = .root
        case .PaySomeoneRecipientAccountView:
            transition = .push
        case .PaySomeoneSourceAccountView:
            transition = .push
        case .PaySomeoneAmountView:
            transition = .push
        case .PaySomeoneTransferScheduleView:
            transition = .push
        case .PaySomeoneTransactionSummaryView:
            transition = .push
        case .PendingTransactionsView:
            transition = .push
        case .PendingTransactionsDetailsView:
            transition = .push
        }
        self.show(from: activity, with: transition, animated: true)
    }
    
    func getViewController(from activity: TransfersActivities)->TransfersRootViewController?{
        self.set(target: .Transfers(activity))
        self.configureModule(activity: activity)
        
        guard let module = _target?.module,
            let viewController = module.activity?.viewController as? TransfersRootViewController,
            let presenter = self._presenter else { return nil }
        
        viewController.set(presenter: presenter)
        
        return viewController
    }
    
}

extension TransfersWireframe{
    func presentSuccessPage(title: String, message: NSAttributedString, doneAction: (()->Void)? = nil){
        let vc = GBASuccessPage()
            .set(title: title)
            .set(message: message)
            .set(doneAction: doneAction)
        
        self.navigator.pushViewController(vc, animated: true)
    }
}
