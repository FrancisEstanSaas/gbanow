//
//  PaySomeoneAmountViewController.swift
//  GBA
//
//  Created by Gladys Prado on 30/11/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation
import UIKit

class PaySomeoneAmountViewController: TransfersRootViewController {
    @IBOutlet weak var txtAmountDeposited: UITextField!
    
    private var currentPresenter: PaySomeonePresenter{
        guard let prsntr = self.presenter as? PaySomeonePresenter else{
            fatalError("Error parsing presenter in ManagePayeeListController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(tempNextButton))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        
        self.addBackButton()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.parent?.title = "Pay Someone"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func backBtn_tapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func addBackButton() {
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    @objc func tempNextButton() {
        if (self.txtAmountDeposited.text != ""){
            let transferAmount = NumberFormatter().number(from: self.txtAmountDeposited.text!)?.doubleValue
            let currentBalance = currentPresenter.sourceAccount!.balance
            
            if (currentBalance >= transferAmount!){
                currentPresenter.transferAmount = transferAmount!
                currentPresenter.didEditForm = true
                self.presenter.wireframe.navigate(to: .PaySomeoneTransferScheduleView, with: self.currentPresenter)
            } else {
                let limitExceededAlert = UIAlertController(title: "Warning", message: "Insufficient balance.", preferredStyle: .alert)
                let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                    //dismiss alert
                }
                
                limitExceededAlert.addAction(confirmAction)
                self.present(limitExceededAlert, animated: true, completion: nil)
            }
        } else {
            let limitExceededAlert = UIAlertController(title: "Warning", message: "Please enter an amount you want to transfer.", preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
                //dismiss alert
            }
            
            limitExceededAlert.addAction(confirmAction)
            self.present(limitExceededAlert, animated: true, completion: nil)
        }
    }
}
