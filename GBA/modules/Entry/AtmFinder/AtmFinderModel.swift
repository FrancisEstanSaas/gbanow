//
//  AtmFinderModel.swift
//  GBA
//
//  Created by Francis Jemuel Bergonia on 06/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import GoogleMaps

struct AtmDetailsStruct {
    
    private(set) public var atmName : String = ""
    private(set) public var atmLocation : String = ""
    private(set) public var atmDistance : String = ""
    //
    
    init(atmName: String, atmLocation: String, atmDistance: String) {
        self.atmName = atmName
        self.atmLocation = atmLocation
        self.atmDistance = atmDistance
    }
}

struct AtmCoordinatesStruct {
    private(set) public var atmLatitude : CLLocationDegrees
    private(set) public var atmLongitude : CLLocationDegrees
    
    init(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        self.atmLatitude = latitude
        self.atmLongitude = longitude
    }
}
