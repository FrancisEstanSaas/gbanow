//
//  EntryRootViewController.swift
//  Prototype-Global_Banking_Access
//
//  Created by Emmanuel Albania on 10/23/17.
//  Copyright © 2017 Emmanuel Albania. All rights reserved.
//

import UIKit

class EntryModuleViewController: RootViewController, GBAFocusableInputViewDelegate{
    
    
    var presenter: EntryRootPresenter{
        get {
            let prsntr = self._presenter as! EntryRootPresenter
            return prsntr
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.apply(gradientLayer: .shadedBlueGreen)
    }
    
    func GBAFocusableInput(view: UIView) {
        self.view.subviews.forEach{
            if let scrollView = $0 as? UIScrollView{
                $0.subviews.first?.subviews.forEach{
                    $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                    if self.focusdObjectFrame != nil {
                        let additionalMeasurements = scrollView.contentSize.height - UIScreen.main.bounds.height
                        additionalMeasurements < 0 ? () : (self.focusdObjectFrame?.size.height += additionalMeasurements)
                        
                        return
                    }
                }
            }else{
                $0 == view ? (self.focusdObjectFrame = $0.frame) : ()
                if self.focusdObjectFrame != nil { return }
            }
        }
    }
}


