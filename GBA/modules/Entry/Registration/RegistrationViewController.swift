
//
//  RegistrationViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/16/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift
import SafariServices
import CryptoSwift

class RegistrationViewController: EntryModuleViewController{
    
    @IBOutlet weak var terms_conditions: UILabel!
    @IBOutlet weak var firstName_textField: GBATitledTextField!
    @IBOutlet weak var lastName_textField: GBATitledTextField!
    @IBOutlet weak var email_textField: GBATitledTextField!
    
    @IBOutlet weak var country_dropDown: GBAPopOverButton!
    @IBOutlet weak var countryCode_textField: GBATitledTextField!
    @IBOutlet weak var mobileNumber_textField: GBATitledTextField!
    
    @IBOutlet weak var password_textField: GBATitledTextField!
    @IBOutlet weak var confirmPassword_textField: GBATitledTextField!
    
    @IBOutlet var requiredFields: [GBATitledTextField]!
    
    private var selectedCountry: Countries = .Philippines
    
    var registrationForm: RegistrationFormEntity{
        return RegistrationFormEntity(firstname: self.firstName_textField.text,
                                      lastname: self.lastName_textField.text,
                                      email: self.email_textField.text,
                                      country_id: self.selectedCountry.id,
                                      mobile: self.countryCode_textField.text + self.mobileNumber_textField.text,
                                      password: self.password_textField.text,
                                      password_confirm: self.confirmPassword_textField.text,
                                      uuid: " ")
    }
    
    var didEditForm = false
    
    var currentPresenter: RegistrationPresenter{
        guard let prsntr = self.presenter as? RegistrationPresenter
            else{ fatalError("Error in parsing presenter for RegistrationViewController") }
        return prsntr
    }
    
    
    
    override func viewDidLoad() {
        self.presenter.set(view: self)
        (self._presenter as! RegistrationPresenter).dataBridge = self
        
        let defaultCountry = Countries.Philippines
        
        self.firstName_textField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: lastName_textField)
            .set(titledFieldDelegate: self)
            .set(placeholder: "First Name")
            .set(validation: .name(message: "Invalid first name"))
        
        
        self.lastName_textField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(next: email_textField)
            .set(placeholder: "Last Name")
            .set(titledFieldDelegate: self)
            .set(validation: .name(message: "Invalid last name"))
        
        self.email_textField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(placeholder: "Email")
            .set(titledFieldDelegate: self)
            .set(next: mobileNumber_textField)
            .set(validation: .email)
        
        self.country_dropDown
            .set(parent: self)
            .set(placeholder: "Country")
            .set(text: defaultCountry.name)
            .set(icon: UIImage(imageLiteralResourceName: defaultCountry.id))
            .set(viewController: GBAPopOverCountryTableview().set(delegate: self))
        
        self.countryCode_textField
            .set(self)
            .set(text: "+63")
            .set(returnKey: .next)
            .set(alignment: .center)
            .set(textFieldIsEditable: false)
        
        self.mobileNumber_textField
            .set(self)
            .set(required: true)
            .set(returnKey: .next)
            .set(viewController: self)
            .set(keyboardType: .phonePad)
            .set(next: password_textField)
            .set(titledFieldDelegate: self)
            .set(placeholder: "Mobile Number")
            .set(validation: .mobileNumberMinimumLength)
        
        self.password_textField
            .set(self)
            .set(required: true)
            .set(security: true)
            .set(returnKey: .next)
            .set(placeholder: "Password")
            .set(titledFieldDelegate: self)
            .set(next: confirmPassword_textField)
            .set(validation: .password)
        
        self.confirmPassword_textField
            .set(self)
            .set(required: true)
            .set(security: true)
            .set(returnKey: .done)
            .set(titledFieldDelegate: self)
            .set(placeholder: "Confirm Password")
        
        self.addBackButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.title = "Registration"
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barStyle = .black
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit",
                                                                 style: .plain,
                                                                 target: self,
                                                                 action: #selector(submit_tapped(_:)))
        
        self.navigationItem.rightBarButtonItem?.tintColor = GBAColor.white.rawValue
        self.view.backgroundColor = GBAColor.white.rawValue
        

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = " "
    }
    
    override func backBtn_tapped() {
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel your registration?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil) }
    
    
    override func addBackButton() {
        
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: String.fontAwesomeIcon(name: .angleLeft), style: .plain, target: self, action: #selector(backBtn_tapped))
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .normal)
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedStringKey.font: UIFont.fontAwesome(ofSize: GBAText.Size.normalNavbarIcon.rawValue)] , for: .highlighted)
        
        UINavigationBar.appearance().barTintColor = GBAColor.primaryBlueGreen.rawValue
        UINavigationBar.appearance().tintColor = GBAColor.white.rawValue
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    @objc private func submit_tapped(_ sender: UIBarButtonItem){
        self.view.subviews.forEach{ $0.resignFirstResponder() }
        
        self.setNewPrimary(userName: self.countryCode_textField.text + self.mobileNumber_textField.text, userPassword: self.confirmPassword_textField.text)
        
        var formIsValid = true
        
        self.requiredFields.forEach{
            print($0.isValid())
            if !$0.isValid(){ formIsValid = false; return }
            
        }
        
        formIsValid ?
            (self.currentPresenter.submitRegistration(registrationForm: registrationForm)) :
            ()
    }
    
    //For Terms and Conditions, and Privacy Policy
    
    @IBAction func checkTermsandConditionsTapped(_ sender: UIButton) {
        let url = URL(string:"http://10.10.10.30/gba-webservice-1.0/public/terms-and-conditions.html")
        
        if let url = url{
            let safariVC =  SFSafariViewController(url: url)
            
            present(safariVC, animated: true)
        }
    
    }
    
    @IBAction func checkPrivacyPolicyTapped(_ sender: UIButton) {
        let url = URL(string:"http://10.10.10.30/gba-webservice-1.0/public/privacy-policy.html")
        
        if let url = url{
            let safariVC =  SFSafariViewController(url: url)
            
            present(safariVC, animated: true)
        }
    
    }
    
    
    
    //for testing only
    func presentVerificationCode(code: String){
        let alert = UIAlertController(title: "CODE", message: code, preferredStyle: .alert)
        
        self.present(alert, animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    //test validation for email address
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"+"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"+"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"+"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"+"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"+"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"+"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func encryptUserData(key: String, iv: String, userData: String) -> String {
        let data = userData.data(using: .utf8)!
        let encrypted = try! AES(key: key, iv: iv).encrypt([UInt8](data))
        let encryptedData = Data(encrypted)
        return encryptedData.base64EncodedString()
    }

    
}

extension RegistrationViewController{
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

extension RegistrationViewController: DataRecievedFromRegistration{
    func didRecievedVerificationData(code: String) {
        self.presentVerificationCode(code: code)
    }
    
}

extension RegistrationViewController: GBAPopOverDelegate{
    func GBAPopOver(didSelect cellView: UITableViewCell) {
        guard let cell = cellView as? GBACountryCellView else { fatalError("Error found in parsing cellView to GBACountryCellView") }
        
        country_dropDown.set(icon: cell.flagImage.image!)
        country_dropDown.set(text: cell.countryLabel.text!)
        self.selectedCountry = cell.country
        
        self.countryCode_textField
            .set(text: cell.countryCode)
            .set(placeholder: "")
    }
}

extension RegistrationViewController: GBAVerificationCodeDelegate{
    func ResendButton_tapped(sender: UIButton) {
        self.currentPresenter.resendVerificationCode{
            var countdown = 60
            
            sender.isEnabled = false
            sender.setTitleColor(GBAColor.darkGray.rawValue, for: .disabled)
            
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                sender.setTitle("RESEND CODE (\(countdown))", for: .disabled)
                
                let _ = countdown-- < 0 ? (timer.invalidate(), (sender.isEnabled = true)): ((),())
            })
        }
    }
    
    func GBAVerification() {
        guard let nav = self.navigationController else { fatalError("Navigation controller was not properly set for RegistrationViewController") }
        DashboardWireframe(nav).presentTabBarController()
    }
}

extension RegistrationViewController: GBATitledTextFieldDelegate{
    func didChange(textField: GBATitledTextField) {
        if textField == self.password_textField{
            self.confirmPassword_textField.set(validation: .matching(with: textField.text))
            self.confirmPassword_textField.isValid()
        }
        self.didEditForm = true
    }
}

extension RegistrationViewController: SetUserLoginInfo {
    
    func addPrimaryLogInUser() {
        print("new Primary User Set")
    }
    
    func setNewPrimary(userName: String, userPassword: String) {
        let newUser = UserKeyInfo()
        newUser.userNumber = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userName)
        newUser.userPassword = self.encryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userPassword)
        do {
            try GBARealm.write {
                GBARealm.add(newUser, update: true)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
}
