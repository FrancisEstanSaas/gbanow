//
//  RegistrationPresenter.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/16/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

protocol DataRecievedFromRegistration{
    func didRecievedVerificationData(code: String)
}

class RegistrationPresenter: EntryRootPresenter{
    
    private var registrationForm: RegistrationFormEntity? = nil
    var dataBridge: DataRecievedFromRegistration? = nil
    
    func submitRegistration(registrationForm: RegistrationFormEntity){
        print(registrationForm)
        self.registrationForm = registrationForm
        guard let bridge = dataBridge else { fatalError("dataBridge was not implemented in RegistrationPresenter") }
        
        self.interactor.remote.Register(form: registrationForm, successHandler: {
            (reply, statusCode) in
            print(reply)
            
            switch statusCode{
            case .dataCreated:
                guard let message = reply["message"] as? String else{
                    fatalError("message not found in server reply: [\(reply)]")
                }
                
                guard let tmp = reply["tmp"] as? [String: Any],
                    let code = tmp["code"] as? String else {
                        print(reply)
                        return
                }
                
                UserAuthentication(json: reply).rewrite()
                
                self.wireframe.presentCodeVerificationViewController(
                    from: self.view as! GBAVerificationCodeDelegate,
                    completion: {
                        guard let nav = self.view.navigationController else{
                            fatalError("NavigationController was not found in RegistrationPresenter")
                        }
                        DashboardWireframe(nav).presentTabBarController()
                },
                    apiCalls:{ code, vc in
                        
                        self.interactor.remote.SubmitRegistrationVerificationCode(code: code, to: registrationForm.mobile    ?? "", successHandler: {
                        (data, statusCode) in
                        vc.serverDidReply(reply: data, statusCode: statusCode)
                            
                        print(data)
                    })
                }, backAction: {
                    self.view.navigationController?.dismiss(animated: true, completion: nil)
                })
                bridge.didRecievedVerificationData(code: "\(message) \(code)")
           
            case .badRequest:
                guard let messages = reply["message"] as? [String:Any] else{
                    fatalError("Message not found")
                }
                
                var message: String?
                
                messages.forEach{
                    message = ($0.value as? [String])?.first
                    return
                }
                
                self.showAlert(with: "Message", message: message ?? "message not found", completion: { () })
            default: break
            }
        })
    }

    
    func resendVerificationCode(callback: @escaping ()->()){
        self.interactor.remote.ResendVerificationCode { (reply, statusCode) in
            switch statusCode{
            case .fetchSuccess, .accepted:
                callback()
                self.showAlert(with: "Notice", message: "Please check your mobile for verification", completion: { } )
            default: break
            }
        }
    }
    
    private func verificationAlert(code: String){
        guard let sender = self.view as? RegistrationViewController else { fatalError("something happened") }
        sender.presentVerificationCode(code: code)
    }
    
    func cancelConfirmation(message:String){
        let alert = UIAlertController(title: "Warning", message: message, preferredStyle: .alert)
        
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.view.navigationController?.dismiss(animated: true, completion: nil)
        }
        
        let no = UIAlertAction(title: "No", style: .cancel) { (_) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(no)
        alert.addAction(yes)
        
        self.view.present(alert, animated: true, completion: nil)
        
    }
}
