//
//  NewPasswordViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/20/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class NewPasswordViewController: EntryModuleViewController{
    
    @IBOutlet weak var newPassword_textField: GBATitledTextField!
    @IBOutlet weak var confirmPassword_textField: GBATitledTextField!
    
    private var currentPresenter: ForgotPasswordPresenter{
        guard let prsntr = self.presenter as? ForgotPasswordPresenter else {
            fatalError("Presenter was not properly set in ForgotPasswordViewController")
        }
        return prsntr
    }
    
    override func viewDidLoad() {
        self.addBackButton()
        self.view.backgroundColor = .white
        self.title = "New Password"
        
        self.currentPresenter.set(view: self)
        
        newPassword_textField
            .set(self)
            .set(required: true)
            .set(security: true)
            .set(validation: .password)
            .set(placeholder: "New Password")
            .set(titledFieldDelegate: self)
        
        confirmPassword_textField
            .set(self)
            .set(security: true)
            .set(required: true)
            .set(titledFieldDelegate: self)
            .set(placeholder: "Confirm Password")
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Submit", style: .plain, target: self, action: #selector(submit_tapped(_:)))
    }
    
    @IBAction func submit_tapped(_ sender: GBAButton) {
        let newPassword = self.newPassword_textField.text
        let confirmNewPassword = self.confirmPassword_textField.text
        
        self.currentPresenter.sendNewPassword(newPassword: newPassword, confirm_password: confirmNewPassword)
        
//      self.presenter.wireframe.presentSuccessPage(title: "SUCCESS", message: NSAttributedString(string: "Password was successfully updated"))
    }
    
    @objc override func backBtn_tapped(){
        
        let alert = UIAlertController(title: "Warning", message: "Are you sure you want to cancel changing your password", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (_) in
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        let no = UIAlertAction(title: "No", style: .default, handler: nil)
        
        alert.addAction(no)
        alert.addAction(yes)
        self.present(alert, animated: true, completion: nil)
        
    }
}

extension NewPasswordViewController:  GBATitledTextFieldDelegate{
    func didChange(textField: GBATitledTextField) {
        if textField == self.newPassword_textField{
            self.confirmPassword_textField.set(validation: .matching(with: textField.text))
            self.confirmPassword_textField.isValid()
        }
    }
    
}








