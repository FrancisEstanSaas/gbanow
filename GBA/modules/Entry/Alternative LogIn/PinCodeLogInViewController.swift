//
//  PinCodeLogInViewController.swift
//  GBA
//
//  Created by EDI on 6/2/18.
//  Copyright © 2018 Republisys. All rights reserved.
//

import UIKit
import KeychainAccess
import CryptoSwift

class PinCodeLogInViewController: EntryModuleViewController, UITextFieldDelegate{

    @IBOutlet weak var profile_imageView: UIImageView!
    @IBOutlet weak var fullName_label: UILabel!
    @IBOutlet weak var pinCodeTextField: UITextField!
    
    var inputPinCode = false
    var numberOnScreen : Double = 0
    var failedAttempt = 0
    
    fileprivate var userKeyInfo: UserKeyInfo{
        get{
            guard let usr = GBARealm.objects(UserKeyInfo.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }
    
    fileprivate var registeredUser: RegisteredUserInfo{
        get{
            guard let usr = GBARealm.objects(RegisteredUserInfo.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }
    
    var currentPresenter: PinCodeLogInPresenter{
        guard let prsntr = self.presenter as? PinCodeLogInPresenter
            else{ fatalError("Error in parsing presenter for RegistrationViewController") }
        return prsntr
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.repopulateProfileInfo()
        self.setBackground()
        (self._presenter as! PinCodeLogInPresenter).dataBridgeToView = self
        self.presenter.set(view: self)
        self.pinCodeTextField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func setBackground(){
        let backgroundImage = UIImage(imageLiteralResourceName: "Login_BG").cgImage
        let layer = CALayer()
        let overlay = CAGradientLayer()
        
        layer.frame = UIScreen.main.bounds
        layer.contents = backgroundImage
        layer.contentsGravity = kCAGravityResizeAspectFill
        
        overlay.set(frame: self.view.bounds)
            .set(start: CGPoint(x: 0, y: 0))
            .set(end: CGPoint(x: 0, y: 1))
            .set(colors: [.white, .primaryBlueGreen])
            .set(locations: [0, 1.4])
            .opacity = 0.8
        
        self.view.layer.insertSublayer(layer, at: 0)
        self.view.layer.insertSublayer(overlay, at: 1)
        
    }

    @IBAction func numbers(_ sender: UIButton)
    {
        if self.inputPinCode == true
        {
            self.pinCodeTextField.text = String(sender.tag)
            self.numberOnScreen = Double(self.pinCodeTextField.text!)!
            self.inputPinCode = false
            if self.pinCodeTextField.text!.count == 6 {
                self.existingPinUserValidator(pin: self.pinCodeTextField.text!)
            } else if self.pinCodeTextField.text!.count > 6 {
                self.showReusableAlert(title: "PIN Code Incorrect", message: "Please Re-Enter your 6 digit PIN")
            }
        }
        else
        {
            self.pinCodeTextField.text = pinCodeTextField.text! + String(sender.tag)
            numberOnScreen = Double(self.pinCodeTextField.text!)!
            if self.pinCodeTextField.text!.count == 6 {
                self.existingPinUserValidator(pin: self.pinCodeTextField.text!)
            } else if self.pinCodeTextField.text!.count > 6 {
                //Please Re-Enter your 6 digit PIN
                self.showReusableAlert(title: "PIN Code Incorrect", message: "Please Re-Enter your 6 digit PIN")
            }
        }
    }
    

    @IBAction func BackTapped(_ sender: UIButton) {
        self.pinCodeTextField.text = ""
    }
    
    
    func pinCodeLogInTapped() {
        
        //For Production
//        let newUser = self.registeredUser
//        let keychainUserMobile = Keychain(service: "userMobile")
//        let keychainPassword = Keychain(service: "password")
//        let userNumber = keychainUserMobile[newUser.userID!]
//        let userPassword = keychainPassword[newUser.userID!]
//        let newUserNumber = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userNumber!)
//        let newUserPassword = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userPassword!)
//
//        let uuidString = UIDevice.current.identifierForVendor!.uuidString  //UUID().uuidString
//        let uuidToSet = "GBA02-"+"\(uuidString)"
//
//        let newForm = LoginFormEntity(mobile: newUserNumber, password: newUserPassword, uuid: uuidToSet)
//        self.currentPresenter.processLogin(form: newForm, controller: self)
        
        //For Demo
        let newUserNumber = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userKeyInfo.userNumber!)
        let newUserPassword = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userKeyInfo.userPassword!)
        let uuidString = UIDevice.current.identifierForVendor!.uuidString  //UUID().uuidString
        let uuidToSet = "GBA02-"+"\(uuidString)"
        
        let newForm = LoginFormEntity(mobile: newUserNumber, password: newUserPassword, uuid: uuidToSet)
        self.currentPresenter.processLogin(form: newForm, controller: self)
    }
    
    func presentDefaultLogin() {
        self.presenter.wireframe.navigate(to: .Loginscreen)
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.presentDefaultLogin()
    }

    func repopulateProfileInfo(){
        let keychainFullName = Keychain(service: "fullName")
        let registeredUserFullName = keychainFullName[registeredUser.userID!]
        let userFullName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: registeredUserFullName!)
        self.fullName_label.text = "\(userFullName)"
    }
    
    
    @IBAction func PRINT(_ sender: UIButton) {
        print(self.pinCodeTextField.text!)
    }
    
    //For Existing PinUsers
    func existingPinUserValidator(pin: String) {
        
        let existingPIN = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userKeyInfo.userPIN!)
        
        let enteredPin = pin
        if enteredPin == existingPIN {
            self.showSuccessAlert()
        } else {
            self.pinCodeTextField.text = ""
            if self.failedAttempt == 5 {
                self.showMaxAttemptAlert()
            } else {
                self.failedAttempt += 1
                self.showReusableAlert(title: "PIN Code Incorrect", message: "Please Re-Enter your 6 digit PIN")
            }
        }
    }
    
    // Alert for the AlertAction()
    func showReusableAlert(title: String, message: String) {
        let alertView = UIAlertController(title: title,
                                          message: message,
                                          preferredStyle:. alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertView, animated: true)
    }
    
    func showSuccessAlert() {
        let alertView = UIAlertController(title: "Login Successful",
                                          message: "Your account will now be processed",
                                          preferredStyle:. alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.pinCodeLogInTapped()}))

        present(alertView, animated: true)
    }
    
    func showMaxAttemptAlert() {
        let alertView = UIAlertController(title: "PIN Code Login Failed",
                                          message: "You have reached maximum number of attempts for PIN Code Login. You will now be directed to our default Login Page",
                                          preferredStyle:. alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.presentDefaultLogin()}))
        present(alertView, animated: true)
    }
    
    //EXP
    func presentVerificationCode(code: String){
        let alert = UIAlertController(title: "CODE", message: code, preferredStyle: .alert)
        
        self.present(alert, animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    func decryptUserData(key: String, iv: String, userData: String) -> String {
        let data = Data(base64Encoded: userData)!
        let decrypted = try! AES(key: key, iv: iv).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
    }

}

extension PinCodeLogInViewController: DataDidReceivedFromPinCodeLogin{
    func didReceiveVerificationData(code: String) {
        //self.presentVerificationCode(code: code)
        //self.showReusableAlert(title: "Notice", message: "Please check your mobile for verification")
    }
}

extension PinCodeLogInViewController: GBAVerificationCodeDelegate{
    func ResendButton_tapped(sender: UIButton) {
        self.currentPresenter.resendVerificationCode{
            
            var countdown = 60
            
            sender.isEnabled = false
            sender.setTitleColor(GBAColor.darkGray.rawValue, for: .disabled)
            
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
                sender.setTitle("RESEND CODE (\(countdown))", for: .disabled)
                
                let _ = countdown-- < 0 ? (timer.invalidate(), (sender.isEnabled = true)): ((),())
            })
        }
    }
    
    func GBAVerification() {
        guard let nav = self.navigationController else{
            fatalError("Navigation View Controller was  not set in LiginViewController")
        }
        DashboardWireframe(nav).presentTabBarController()
    }
}
