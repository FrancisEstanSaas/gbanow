//
//  OnboardingPageViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/8/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import FontAwesome_swift

class OnboardingPageViewController: UIPageViewController{
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    var orderedViewController: [OnboardingViewController] = [OnboardingViewController]()
    
    let contents = OnboardingData.contents
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        
        guard let nav = self.navigationController
            else { fatalError("NavigationController not found in OnboardingPageViewController") }
        
        let wireframe = EntryWireframe(nav)
        let interactor: (remote: EntryRemoteInteractor, local: RootLocalInteractor) = (EntryRemoteInteractor(),RootLocalInteractor())
        
        for content in contents{
            let vc = Bundle.main.loadNibNamed("OnboardingViewController", owner: self, options: nil)?.first as! OnboardingViewController
            let presenter = OnboardingPresenter(wireframe: wireframe, view: vc)
            presenter.interactor = interactor
            
            vc.mainImage.image = content.image
            vc.mainContent.text = content.title
            vc.subContent.text = content.description
            vc.set(presenter: presenter)
            
            orderedViewController.append(vc)
        }
        
        guard let primaryViewController = orderedViewController.first else { fatalError("No onboarding data was found") }
        
        self.setViewControllers([primaryViewController], direction: .forward, animated: false, completion: nil)
        
        self.view.apply(gradientLayer: .shadedBlueGreen)
    }
    
}

extension OnboardingPageViewController: UIPageViewControllerDataSource{
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentVC = viewController as? OnboardingViewController,
            let index = orderedViewController.index(of: currentVC) else { fatalError("Error in parsing next viewController") }
        
        if index != 0 { return orderedViewController[index - 1] }
        else { return nil }
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentVC = viewController as? OnboardingViewController,
            let index = orderedViewController.index(of: currentVC) else { fatalError("Error in parsing next viewController") }
        
        if index < orderedViewController.count - 1 {
            let vc = orderedViewController[index + 1]
            
            if index + 1 == orderedViewController.count - 1{ vc.btnExit.setTitle("Done", for: .normal)}
            vc.view.isUserInteractionEnabled = false
            return vc
        }
        else { return nil }
    }
}

