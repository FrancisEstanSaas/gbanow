//
//  OnboardingViewController.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/8/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

class OnboardingViewController: EntryModuleViewController{
    
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var mainContent: UILabel!
    @IBOutlet weak var subContent: UILabel!
    @IBOutlet weak var btnExit: UIButton!
    
    fileprivate var user: UserKeyInfo{
        get{
            guard let usr = GBARealm.objects(UserKeyInfo.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.btnExit.addTarget(self, action: #selector(self.btnExit_tapped(_:)), for: .touchUpInside)
        self.view.isUserInteractionEnabled = true
    }
    
    @objc func btnExit_tapped(_ sender: UIButton) {
        self.presenter.wireframe.navigate(to: .Loginscreen)
        UserDefaults.standard.set(true, forKey: "isNotFirstOpened")
    }
}
