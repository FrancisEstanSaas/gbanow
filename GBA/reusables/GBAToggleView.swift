//
//  GBAToggleView.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/19/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

enum GBAToggle{
    case on, off
}

class GBAToggleView:UIView{
    
    var toggle: GBAToggle = .off{
        didSet{
            switch toggle {
            case .on:
                self.backgroundColor = GBAColor.primaryBlueGreen.rawValue
                self.layer.borderColor = GBAColor.primaryBlueGreen.rawValue.cgColor
                self.layer.opacity = 1
            case .off:
                self.backgroundColor = GBAColor.white.rawValue
                self.layer.borderColor = GBAColor.gray.rawValue.cgColor
                self.layer.opacity = 0.7
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layoutView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layoutView()
    }
    
    private func layoutView(){
        let length = self.height < self.width ? self.height: self.width
        self.layer.cornerRadius = length / 2
        self.layer.borderWidth = 1.5
        self.layer.opacity = 0.7
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        self.layoutView()
    }
}
