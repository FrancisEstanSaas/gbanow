//
//  GBATextField.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/9/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

protocol GBATitledTextFieldDelegate{
    func didChange(textField: GBATitledTextField)
}

enum GBATitledTextFieldInputType{
    case freeText
    case mobileNumber
    case expiryDate
    case cardCVV
}
 
class GBATitledTextField: GBARootFocusableObject, UITextFieldDelegate{
    private var _title: UILabel = UILabel()
    private var _textField: UITextField = UITextField()
    private var _validationTitle: UILabel = UILabel()
    
    fileprivate var delegate: GBAFocusableInputViewDelegate? = nil
    fileprivate var titledFieldDelegate: GBATitledTextFieldDelegate? = nil
    fileprivate var _nextTextField: GBARootFocusableObject? = nil
    fileprivate var inputType: GBATitledTextFieldInputType = .freeText
    fileprivate var viewController: EntryModuleViewController? = nil
    
    //validation rules
    fileprivate var maxLength: Int?
    fileprivate var isValidField = true
    fileprivate var isEqualTo: Bool = false
    fileprivate var isRequiredField: Bool = false
    fileprivate var validation: GBAValidation? = nil
    
    fileprivate(set) var placeholder: String = ""{
        didSet{
            self._title.text = self.placeholder
        }
    }
    
    internal var text: String{
        get{
            guard let txt = self._textField.text else { return "" }
            return txt
        }
    }
    
    var textFieldFrame: CGPoint{
        get{
            return _textField.convert(_textField.center, from: self.superview)
        }
    }
    
    private let underline = UIView()
    fileprivate var underlineColor: GBAColor = GBAColor.lightGray{
        didSet{ self.underline.backgroundColor = self.underlineColor.rawValue }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layoutTextField()
    }
    
    override func becomeFirstResponder() -> Bool {
        super.becomeFirstResponder()
        
        return self._textField.becomeFirstResponder()
    }
    
    private func layoutTextField(){
        self.backgroundColor = .clear
        
        _title = UILabel()
            .set(color: GBAColor.gray.rawValue)
            .set(lines: 1)
            .set(size: 14)
            .add(to: self)
        
        _textField = UITextField()
            .set(fontColor: GBAColor.black.rawValue)
            .set(font: GBAText.Font.main(GBAText.Size.subContent.rawValue).rawValue)
            .set(backgroundColor: .clear)
        
        _validationTitle = UILabel()
            .set(color: GBAColor.red.rawValue)
            .set(lines: 0)
            .set(size: 10)
            .add(to: self)

        _textField.delegate = self
        _textField.isUserInteractionEnabled = true
        _textField.translatesAutoresizingMaskIntoConstraints = false
        _textField.addTarget(self, action: #selector(valueChanged), for: UIControlEvents.editingChanged)
        
        underline.backgroundColor = self.underlineColor.rawValue
        underline.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(underline)
        self.addSubview(_textField)
        
        _textField.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable()
        _textField.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
        _textField.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).Enable()
        _textField.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.6).Enable()
        
        _title.topAnchor.constraint(equalTo: _textField.topAnchor).Enable()
        _title.bottomAnchor.constraint(equalTo: _textField.bottomAnchor).Enable()
        _title.leadingAnchor.constraint(equalTo: _textField.leadingAnchor).Enable()
        _title.trailingAnchor.constraint(equalTo: _textField.trailingAnchor).Enable()
        
        underline.heightAnchor.constraint(equalToConstant: 1).Enable()
        underline.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable()
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -14).Enable()
        
        //set constraints for validations here
        _validationTitle.bottomAnchor.constraint(equalTo: self.bottomAnchor).Enable()
        _validationTitle.leadingAnchor.constraint(equalTo: self.leadingAnchor).Enable()
        _validationTitle.trailingAnchor.constraint(equalTo: self.trailingAnchor).Enable()
        _validationTitle.topAnchor.constraint(equalTo: underline.topAnchor, constant: 3).Enable()
        
        self.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self._textField.returnKeyType != .next{
            return textField.resignFirstResponder()
        }
        guard let nextTextField = self._nextTextField,
            let parent = self.superview
            else { return textField.resignFirstResponder() }
        
        self._textField.resignFirstResponder()
        
        Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
            parent.subviews.forEach{
                if $0 == nextTextField {
                    $0.becomeFirstResponder(); return
                }
            }
        }
        
        self.validateField()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.inputType == .mobileNumber && textField.text == "+" && string == ""{
            return false
        }
        
        switch self.inputType{
        case .expiryDate:
            var originalText = textField.text
            let replacementText = string.replacingOccurrences(of: " ", with: "")
            
            if range.length > 0 { return true }
            if string == "" || range.location > 4{ return false }
            
            if !CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: replacementText)) {
                return false
            }
            
            if range.location == 2 {
                originalText?.append("/")
                textField.text = originalText
            }
            return true
        case .cardCVV:
            if range.length > 0 { return true }
            if string == "" || range.location > 3{ return false }
            
            return true
        default: break
        }
        
        guard let length = self.maxLength,
            let currentString = self._textField.text as NSString?
            else { return true }
        
        
        return currentString.length < length
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.inputType == .mobileNumber && textField.text == ""{
            self.set(text: "+")
        }
        
        guard let del = self.delegate else { fatalError("Delegate was not set for GBATitledTextField") }
        del.GBAFocusableInput(view: self)
        return true
        
    }
    
    @objc func valueChanged(){
        self.validateField()
        
        self.layoutIfNeeded()
        
        guard let textFieldDelegate = self.titledFieldDelegate else { return }
        textFieldDelegate.didChange(textField: self)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if self.inputType == .mobileNumber && textField.text == "+"{ self.set(text: "") }
        self.validateField()
        return true
    }
    
    private func validateField(){
        
        guard let string = self._textField.text else { return }
        var message = ""
        self._validationTitle.isHidden = true
        self.underline.backgroundColor = self.underlineColor.rawValue
        
        validate: if self._textField.text != ""{ UIView.animate(withDuration: 0.5, animations: { self._title.frame.origin.y = -8 } ) }
        else { UIView.animate(withDuration: 0.5, animations: { self._title.frame.origin.y = self._textField.frame.origin.y }) }
        
        if self.isRequiredField{
            let validation = GBAValidation.required
            self.isValidField = self.matches(from: validation.regex[0], in: string)
            if !isValidField { message = validation.errorMessage[0] }
        }
        
        validate: for index in 0..<(validation?.regex.count ?? 0){
            if !self.isValidField { break validate }
            let errorMessage = validation?.errorMessage[index]
            
            switch self.validation!{
            case .matching(let password): self.isValidField = password == string
            default:
                guard let regex = validation?.regex[index] else{ break validate }
                self.isValidField = self.matches(from: regex, in: string)
            }
            
            if !self.isValidField { message = errorMessage ?? "****" }
        }
        
        if !self.isValidField {
            self._validationTitle.text = message
            self._validationTitle.isHidden = false
            self.underline.backgroundColor = GBAColor.red.rawValue
        }
    }
    
    public func hideValidation(){
        self._validationTitle.text = ""
        self._validationTitle.isHidden = true
        self.underline.backgroundColor = self.underlineColor.rawValue
        self.resignFirstResponder()
    }
    
    @discardableResult public func isValid()->Bool{
        self.validateField()
        return self.isValidField
    }
    
    fileprivate func matches(from regex: String, in string: String) -> Bool{
        do {
            let result = try NSRegularExpression(pattern: regex).matches(in: string, range: NSRange(string.startIndex..., in: string))
            return !result.isEmpty
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return false
        }
    }

}

extension GBATitledTextField{
    @discardableResult
    func set(placeholder: String)->Self{
        self.placeholder = placeholder
        return self
    }
    
    @discardableResult
    func set(text: String)->Self{
        self._textField.text = text
        self.valueChanged()
        return self
    }
    
    @discardableResult
    func set(alignment: NSTextAlignment)->Self{
        self._textField.textAlignment = alignment
        return self
    }
    
    @discardableResult
    func set(keyboardType: UIKeyboardType)->Self{
        self._textField.keyboardType = keyboardType
        return self
    }
    
    @discardableResult
    func set(inputType: GBATitledTextFieldInputType)->Self{
        self.inputType = inputType
        
        switch self.inputType{
        case .freeText:
            self.set(keyboardType: .default)
        case .mobileNumber, .expiryDate, .cardCVV:
            self.set(keyboardType: .numberPad)
        }
        
        return self
    }
    
    @discardableResult
    func set(textColor: GBAColor)->Self{
        self._textField.textColor = textColor.rawValue
        return self
    }
    
    @discardableResult
    func set(underlineColor: GBAColor)->Self{
        self.underlineColor = underlineColor
        return self
    }
    
    @discardableResult
    func set(security: Bool)->Self{
        self._textField.isSecureTextEntry = security
        return self
    }
    
    @discardableResult
    func set(max characterLength: Int)->Self{
        self.maxLength = characterLength
        return self
    }
    
    @discardableResult
    func set(_ delegate: GBAFocusableInputViewDelegate)->Self{
        self.delegate = delegate
        return self
    }
    
    @discardableResult
    func set(textFieldIsEditable: Bool)->Self{
        self._textField.isEnabled = textFieldIsEditable
        return self
    }
    
    @discardableResult
    func set(returnKey: UIReturnKeyType)->Self{
        self._textField.returnKeyType = returnKey
        return self
    }
    
    @discardableResult
    func set(next textField: GBARootFocusableObject)->Self{
        self._textField.returnKeyType = .next
        self._nextTextField = textField
        return self
    }
    
    @discardableResult
    func set(returnKeyType: UIReturnKeyType)->Self{
        self._textField.returnKeyType = returnKeyType
        return self
    }
    
    @discardableResult
    func set(titledFieldDelegate: GBATitledTextFieldDelegate)->Self{
        self.titledFieldDelegate = titledFieldDelegate
        return self
    }
    
    @discardableResult
    func set(viewController: EntryModuleViewController)->Self{
        self.viewController = viewController
        return self
    }
    
    //for validations
    @discardableResult
    func set(required: Bool)->Self{
        self.isRequiredField = required
        return self
    }
    
    @discardableResult
    func set(validation: GBAValidation)->Self{
        self.validation = validation
        return self
    }
    
    
    @discardableResult
    func set(userInteractionEnabled status: Bool)->Self{
        self._textField.isUserInteractionEnabled = status
        return self
    }
}

 
 
