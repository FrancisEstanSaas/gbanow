//
//  GBASuccessPage.swift
//  GBA
//
//  Created by Emmanuel Albania on 12/1/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit

protocol GBASuccessPageDelegate {
    func didTapDone()
}

class GBASuccessPage:RootViewController{
    
    fileprivate(set) var _title: String?{
        didSet{
            self.title = self._title
        }
    }
    
    fileprivate(set) var _message: NSAttributedString?{
        didSet{ self.message_label.attributedText = self._message }
    }
    
    fileprivate let successImage = UIImageView(image: UIImage(named: "Success_Icon"))
    private let success_label = UILabel()
    fileprivate(set) var message_label = UILabel()
    fileprivate(set) var doneAction:(()->Void)? = nil
    
    override func viewDidLoad() {
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneBtn_tapped))
        self.navigationItem.hidesBackButton = true
        
        self.view.backgroundColor = .white
        self.successImage.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.successImage)
        
        success_label
            .set(lines: 1)
            .set(alignment: .center)
            .set(fontStyle: GBAText.Font.main(GBAText.Size.title.rawValue).rawValue)
            .set(value: "SUCCESS!")
            .add(to: self.view)
        
        message_label
            .set(lines: 0)
            .set(alignment: .center)
            .set(fontStyle: GBAText.Font.main(GBAText.Size.subContent.rawValue).rawValue)
            .add(to: self.view)
        
        self.successImage.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor, constant: 30).Enable()
        self.successImage.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 100).Enable()
        self.successImage.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -100).Enable()
        self.successImage.heightAnchor.constraint(equalTo: self.successImage.widthAnchor).Enable()
        
        self.success_label.topAnchor.constraint(equalTo: self.successImage.bottomAnchor, constant: 30).Enable()
        self.success_label.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).Enable()
        self.success_label.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).Enable()
        
        self.message_label.topAnchor.constraint(equalTo: self.success_label.bottomAnchor, constant: 10).Enable()
        self.message_label.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).Enable()
        self.message_label.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).Enable()
    }
    
    @objc private func doneBtn_tapped(){
        guard let action = self.doneAction else{
            self.navigationController?.popToRootViewController(animated: true)
            return
        }
        (action)()
    }
}

extension GBASuccessPage{
    @discardableResult
    func set(title: String)->Self{
        self._title = title
        return self
    }
    
    @discardableResult
    func set(message: NSAttributedString)->Self{
        self._message = message
        return self
    }
    
    @discardableResult
    func set(doneAction: (()->Void)? = nil)->Self{
        self.doneAction = doneAction
        return self
    }
}
