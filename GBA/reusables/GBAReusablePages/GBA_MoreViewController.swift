//
//  GBA_More.swift
//  GBA
//
//  Created by Emmanuel Albania on 11/27/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import UIKit
import RealmSwift
import CryptoSwift

class GBA_MoreViewController: RootViewController{
    
    var delegate: PinCodeVerificationDelegate? = nil
    @IBOutlet weak var headerContainerView: UIView!
    @IBOutlet weak var transactionHistory_btn: More_optionView!
    @IBOutlet weak var managePayees_btn: More_optionView!
    @IBOutlet weak var customerSupport_btn: More_optionView!
    @IBOutlet weak var knowYourCustomerBtn: More_optionView!
    @IBOutlet weak var settings_btn: More_optionView!
    @IBOutlet weak var logout_btn: More_optionView!
    @IBOutlet weak var btnViewProfile: UIButton!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var userFullName: UILabel!
    
    
    //Realm Q.Variables 
    fileprivate var user: User{
        get{
            guard let usr = GBARealm.objects(User.self).first else{
                fatalError("User not found")
            }
            return usr
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nav = UINavigationController()
        nav.navigationBar.barTintColor = GBAColor.primaryBlueGreen.rawValue
        nav.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: GBAColor.white.rawValue]
        nav.navigationBar.tintColor = GBAColor.white.rawValue
        nav.isNavigationBarHidden = false
        
        self.headerContainerView.apply(gradientLayer: .shadedBlueGreen)
        
        
        transactionHistory_btn
            .set(title: "Transaction History")
            .set(icon: UIImage(named: "Sidebar_Transaction_History")!)
            .setAction(target: self) {
                TransactionHistoryWireframe(nav).navigate(to: .TransactionHistoryscreen)
                self.present(nav, animated: true, completion: nil)
        }
        
        managePayees_btn
            .set(title: "Manage Payees")
            .set(icon: UIImage(named: "Sidebar_Payees")!)
            .setAction(target: self) {
            ManagePayeesWireframe(nav).navigate(to: .ManagePayeeListscreen)
                self.present(nav, animated: true, completion: nil)
        }
        
        customerSupport_btn
            .set(title: "Customer Support")
            .set(icon: UIImage(named: "Sidebar_Customer_Support")!)
            .setAction(target: self) {
                EntryWireframe(nav).navigate(to: .Supportscreen)
                self.present(nav, animated: true, completion: nil)
        }

        knowYourCustomerBtn
            .set(title: "Know Your Customer")
            .set(icon: UIImage(named: "Sidebar_Customer_Support")!)
            .setAction(target: self) {
                KYCWireframe(nav).navigate(to: .PersonalDetailsVC)
                self.present(nav, animated: true, completion: nil)
        }

        self.present(nav, animated: true, completion: nil)
        settings_btn
            .set(title: "Settings")
            .set(icon: UIImage(named: "Sidebar_Settings")!)
            .setAction(target: self) {
                SettingsWireframe(nav).navigate(to: .AccessSettingsMainView)
                self.present(nav, animated: true, completion: nil)
        }
        
        logout_btn
            .set(title: "Logout")
            .set(icon: UIImage(named: "Sidebar_LogOut")!)
            .setAction(target: self) {
                do{
                    try UserAuthentication().truncate()
                    try User().truncate()
                    try Payee().truncate()
                    try Wallet().truncate()
                    //try UserKeyInfo().truncate()
                    //try RegisteredUser().truncate()
                }catch{ print(error.localizedDescription) }
                EntryWireframe(nav).navigate(to: .Loginscreen)
                self.present(nav, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.repopulateProfileInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.view.frame = UIScreen.main.bounds
        self.view.layoutIfNeeded()
    }
        
    @IBAction func didTapProfile(_ sender: Any) {
        let nav = UINavigationController()
        SettingsWireframe(nav).navigate(to: .EditProfileView)
        self.present(nav, animated:true, completion: nil)
    }
    
    func decryptUserData(key: String, iv: String, userData: String) -> String {
        let data = Data(base64Encoded: userData)!
        let decrypted = try! AES(key: key, iv: iv).decrypt([UInt8](data))
        let decryptedData = Data(decrypted)
        return String(bytes: decryptedData.bytes, encoding: .utf8) ?? "Could not decrypt"
    }
    
    private func repopulateProfileInfo(){
        let userProfile = self.user
        
        let firstName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.firstname)
        let lastName = self.decryptUserData(key: LockGenerator.key.value, iv: LockGenerator.iv.value, userData: userProfile.lastname)
        
        
        let userFullName = "\(firstName) \(lastName)"
        
        self.userFullName.text = userFullName
    }
    

}

