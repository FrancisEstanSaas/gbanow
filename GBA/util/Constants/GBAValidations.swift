//
//  GBAValidations.swift
//  GBA
//
//  Created by Gladys Prado on 8/12/17.
//  Copyright © 2017 Republisys. All rights reserved.
//

import Foundation

enum GBAValidation{
    case none
    case email
    case password
    case required
    case alphaNumeric
    case name(message: String)
    case mobileNumberMinimumLength
    case matching(with: String)
    
    var regex: [String] {
        switch self {
        case .none:                             return []
        case .email:                            return [emailRegex]
        case .name(_):                          return ["^[a-zA-Z\\s]+$"]
        case .required:                         return ["\\S"]
        case .password:                         return passwordRegex
        case .alphaNumeric:                     return ["^[a-zA-Z0-9]*$"]
        case .matching(let string):             return ["\(string)"]
        case .mobileNumberMinimumLength:        return ["^[0-9]{8,}$"]
        }
    }
    
    var errorMessage: [String]{
        switch self{
        case .none:                             return []
        case .email:                            return ["Invalid email address"]
        case .password:                         return passwordErrorMessages
        case .required:                         return ["Required field"]
        case .matching(_):                      return ["Passwords do not match"]
        case .alphaNumeric:                     return [""]
        case .name(let message):                return [message]
        case .mobileNumberMinimumLength:        return ["Should contain at least 8 characters"]
        }
    }
}

private let emailRegex              =    "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}" +
                                            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
                                            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" +
                                            "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" +
                                            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
                                            "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
                                            "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"

private let passwordRegex           =   [   ".*[A-Z]+.*",
                                            ".*[a-z]+.*",
                                            ".*[0-9]+.*",
                                            "^.{8,}$"  ]

private let passwordErrorMessages    =  [   "Password should contain at least 1 Uppercase.",
                                            "Password should contain at least 1 Lowercase.",
                                            "Password should contain at least 1 number.",
                                            "Password should contain at least 8 characters in length."     ]









