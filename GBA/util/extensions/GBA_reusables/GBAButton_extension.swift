



import UIKit

extension GBAButton{
    @discardableResult
    func setNormal(title: String)->Self{
        self.setTitle(title, for: .normal)
        return self
    }
    
    @discardableResult
    func setNormal(foreColor: GBAColor)->Self{
        self.setTitleColor(foreColor.rawValue, for: .normal)
        return self
    }
    
    @discardableResult
    func set(backgroundColor: GBAColor)->Self{
        self.backgroundColor = backgroundColor.rawValue
        return self
    }
}

