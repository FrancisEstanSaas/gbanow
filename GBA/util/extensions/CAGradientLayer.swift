



import UIKit

extension CAGradientLayer{
    
    @discardableResult
    func set(colors: [GBAColor])->Self{
        self.colors = colors.map{ return $0.rawValue.cgColor }
        return self
    }
    
    @discardableResult
    func set(end point: CGPoint)->Self{
        self.endPoint = point
        return self
    }
    
    @discardableResult
    func set(start point: CGPoint)->Self{
        self.startPoint = point
        return self
    }
    
    @discardableResult
    func set(frame: CGRect)->Self{
        self.frame = frame
        return self
    }
    
    @discardableResult
    func set(locations: [NSNumber])->Self{
        self.locations = locations
        return self
    }
}
