//
//  Operators.swift
//  GBA
//
//  Created by Republisys on 31/01/2018.
//  Copyright © 2018 Republisys. All rights reserved.
//

postfix operator ++
postfix operator --

@discardableResult postfix func ++(value: inout Int) -> Int {
    value+=1
    return value
}

@discardableResult postfix func --(value: inout Int) -> Int {
    value-=1
    return value
}
